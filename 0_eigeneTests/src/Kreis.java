
public class Kreis extends GeoObject {

	// Attribute

	private double radius;
	private final double PI = 3.14159;

	// Konstruktoren

	public Kreis(double x, double y, double radius) {
		super(x, y);
		this.radius = radius;
	}

	// Methoden

	@Override
	public double determineArea() {
		return PI * this.radius * this.radius;
	}

	@Override
	public String toString() {
		return super.toString() + "; Kreis (Radius = " + this.radius + ")";
	}

}
