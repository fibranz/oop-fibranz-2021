
public class Uebergabemechanismen {

	public static void main(String[] args) {

		String str1 = "huhu";
		String str2 = str1;
		int zahl1 = 9;
		Kreis kreis1 = new Kreis(1, 2, 3);
		Kreis kreis2 = kreis1;

		System.out.printf("%s, %s, %d%n", str1, str2, zahl1);
		test(str1, zahl1);
		System.out.printf("%s, %s, %d%n", str1, str2, zahl1);

		str1 = "gruezi";
		System.out.printf("%s %s%n", str1, str2);

		kreis1.setX(22);
		System.out.println(kreis1 + " | " + kreis2);

	}

	public static void test(String str, int i) {
		System.out.printf("test: %s, %d%n", str, i);
		str = "gruezi";
		i = 17;
		System.out.printf("test: %s, %d%n", str, i);
	}

}
