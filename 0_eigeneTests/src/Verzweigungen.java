
public class Verzweigungen {
	public static void main(String[] args) {

		int zahl = 0;
		System.out.println(zahl++);// 0; gibt zahl aus, inkrementiert dann zahl
		System.out.println(zahl);// 1
		System.out.println(++zahl);// 2; inkrementiert zahl, gibt dann zahl aus
		// bzw. gibt zahl+1 aus und inkrementiert dann zahl
		System.out.println(zahl);// 2
		System.out.println("-----------------");

		System.out.println(3 / 4);
		System.out.println(3.0 / 4);
		System.out.println(3.0f / 4);
		System.out.println(3f / 4);
		System.out.println("-----------------");

		// **** Test zu Kontrollstrukturen (Verzweigungen) ****

		int a = 2, b = 3;

		// ** Variante 1 **
		if (a > b)
			System.out.println("a groesser b");
		else if (a > 0)
			System.out.println("a groesser 0");
		else
			System.out.println("a kleiner oder gleich 0");

		// ** Variante 2 **
		if (a > b) {
			System.out.println("a groesser b");
		} else if (a > 0)
			System.out.println("a groesser 0");
		else
			System.out.println("a kleiner oder gleich 0");

		// ** Variante 3 **
		if (a > b) {
			System.out.println("a groesser b");
		} else {
			if (a > 0) {
				System.out.println("a groesser 0");
				System.out.println("Test");
			} else
				System.out.println("a kleiner oder gleich 0");
		}

		System.out.println("\n----------------------");

		char buchstabe = 'd';
		switch (buchstabe) {
		case 'a':
			System.out.println("a");
		case 'b':
			System.out.println("b");
		case 'c':
			System.out.println("c");
			break;
		case 'd':
			System.out.println("d");
		default:
			System.out.println("nenene");
//			break;
		}
		System.out.println("switch zuende");
	}

}
