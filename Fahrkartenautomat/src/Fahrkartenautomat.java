﻿//import java.util.InputMismatchException;
import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		int zuZahlenderBetrag = 0;
		int rueckgeld = 0;

		while (true) {

			// **** Phase 1: Bestellung erfassen ****

			zuZahlenderBetrag = fahrkartenbestellungErfassen();

			// **** Phase 2: Geldeinwurf ****

			rueckgeld = fahrkartenBezahlen(zuZahlenderBetrag);

			// **** Phase 3: Fahrscheinausgabe ****

			fahrkartenAusgeben();

			// **** 4. Phase: Rueckgeldberechnung und -Ausgabe ****

			rueckgeldAusgeben(rueckgeld);
		}

	}
	
	public static void trennlinieZeichnen(int breite, char zeichen) {
		for(int k = 0; k < breite; k++) {
			System.out.print(zeichen);
		}
		System.out.println();
		return;
	}
	
	public static void eingerahmterText(String text, int breite, char zeichen) {
		trennlinieZeichnen(breite, zeichen);
		boolean istTextGeschrieben = false;
		for(int k = 0; k < breite; k++) {
			int teilbreite = (breite - text.length())/2;
			if(k < teilbreite) System.out.print(" ");
			else if((k < (teilbreite + text.length())) && (istTextGeschrieben == false)) {
				System.out.print(text);
				istTextGeschrieben = true;
			}
			else System.out.print(" ");
		}
		System.out.println();
		trennlinieZeichnen(breite, zeichen);
		return;
	}

	public static int fahrkartenbestellungErfassen() {
		Scanner myScanner = new Scanner(System.in);
		int wunsch = 1;
		int anzahl = 1;
		int summe = 0;
		String[] bezeichnung = {
				"Einzelfahrschein Berlin AB",
				"Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC",
				"Kurzstrecke",
				"Tageskarte Berlin AB",
				"Tageskarte Berlin BC",
				"Tageskarte Berlin ABC",
				"Kleingruppen-Tageskarte Berlin AB",
				"Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC"
		};
		int[] preisInCt = {
				290,
				330,
				360,
				190,
				860,
				900,
				960,
				2350,
				2430,
				2490
		};
		
		eingerahmterText("Guten Tag!", 70, '=');
		
//		System.out.print("Zu zahlender Betrag [EUR]: ");
//		// Typkonversion schneidet Nachkommastellen stets ab;
//		// zur Vermeidung von Rundungsfehlern: mathemat. Runden, d.h.
//		// Kommazahl auf naheliegendste Zahl (double) runden, die einer Ganzzahl (int)
//		// gleicht
//		wunsch = (int) Math.rint(100 * myScanner.nextDouble());
//		myScanner.nextLine();
//		if (wunsch < 0) {
//			wunsch = Math.abs(wunsch);
//			System.out.printf("Gewählter Einzelpreis: %.2f EUR%n", ((double) wunsch) * 1e-2);
//		}

		System.out.println("Sie können zwischen folgenden Tarifen wählen: ");
		for(int k = 0; k < bezeichnung.length; k++) {
			System.out.printf("(%3d) %-50s %6.2f EUR%n", k+1, bezeichnung[k], ((double) (preisInCt[k]) * 1e-2));
		}
		System.out.println();
//		System.out.printf("| %-40s |%10s | Taste %d%n", "Einzelfahrschein Regeltarif AB", "2,90 EUR", 1);
//		System.out.printf("| %-40s |%10s | Taste %d%n", "Tageskarte Regeltarif AB", "8,60 EUR", 2);
//		System.out.printf("| %-40s |%10s | Taste %d%n", "Kleingruppen-Tageskarte Regeltarif AB", "23,50 EUR", 3);
		do {
			System.out.print("Geben Sie Ihre Bestellung ein: ");
			wunsch = myScanner.nextInt();
			myScanner.nextLine();
			if (wunsch < 1 || wunsch > bezeichnung.length) {
				System.out.println("<<< falsche Eingabe >>>");
			}
			else System.out.printf("Ihre Wahl: (%d) %s%n", wunsch, bezeichnung[wunsch-1]);
		} while (wunsch < 1 || wunsch > bezeichnung.length);

		do {
			System.out.print("Anzahl der Tickets (max. 10): ");
			anzahl = myScanner.nextInt();
			myScanner.nextLine();
			if (anzahl < 1 || anzahl > 10) {
				System.out.println("<<< falsche Eingabe >>>");
			}
		} while (anzahl < 1 || anzahl > 10);

//		switch (wunsch) {
//		case 1:
//			summe = anzahl * 290;
//			break;
//		case 2:
//			summe = anzahl * 860;
//			break;
//		case 3:
//			summe = anzahl * 2350;
//			break;
//		}
//		if (wunsch == 1)
//			summe = anzahl * 290;
//		if (wunsch == 2)
//			summe = anzahl * 860;
//		if (wunsch == 3)
//			summe = anzahl * 2350;

//		return summe;// Preis [ct] zurueck
		return anzahl * preisInCt[wunsch-1];// Preis [ct] zurueck
	}

	public static int fahrkartenBezahlen(int zuZahlen) {
		Scanner myScanner = new Scanner(System.in);
		int bereitsEingezahlt = 0;
		int eingeworfeneMuenze = 0;
		while (bereitsEingezahlt < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f EUR%n", ((double) (zuZahlen - bereitsEingezahlt)) * 1e-2);
			System.out.print("Eingabe [EUR] (mind. 5 Cent, max. 2 Euro): ");
			eingeworfeneMuenze = (int) (100 * myScanner.nextDouble());
			myScanner.nextLine();
			bereitsEingezahlt += eingeworfeneMuenze;
		}
		return bereitsEingezahlt - zuZahlen;// Rueckgeld [ct]
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(500);// 500ms warten
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(int rueckgeldInCent) {
		if (rueckgeldInCent > 0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EUR%n", ((double) rueckgeldInCent) / 100.0);
			System.out.println("wird in folgenden Münzen ausgezahlt:");
			muenzeAusgeben(rueckgeldInCent, "EUR");
		} else {
			System.out.println("Sie haben passend bezahlt, danke.");
		}
		System.out.println("\nDenken Sie daran, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n");
	}

	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		while (betrag >= 200)// 2-Euro-Stk
		{
//			System.out.printf("%.2f %s%n", 2.0, einheit);
			System.out.printf("%d,-  %s%n", 2, einheit);
			betrag -= 200;
		}
		while (betrag >= 100)// 1-Euro-Stk
		{
			System.out.printf("%d,-  %s%n", 1, einheit);
			betrag -= 100;
		}
		while (betrag >= 50)// 50-ct-Stk
		{
			System.out.printf("-,%02d %s%n", 50, einheit);
			betrag -= 50;
		}
		while (betrag >= 20)// 20-ct-Stk
		{
			System.out.printf("-,%02d %s%n", 20, einheit);
			betrag -= 20;
		}
		while (betrag >= 10)// 10-ct-Stk
		{
			System.out.printf("-,%02d %s%n", 10, einheit);
			betrag -= 10;
		}
		while (betrag > 0)// 5-ct-Stk
		{
			System.out.printf("-,%02d %s%n", 5, einheit);
			betrag -= 5;
		}
	}
}