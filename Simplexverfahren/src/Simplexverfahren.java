import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.math3.optim.MaxIter;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.linear.LinearConstraint;
import org.apache.commons.math3.optim.linear.LinearConstraintSet;
import org.apache.commons.math3.optim.linear.LinearObjectiveFunction;
import org.apache.commons.math3.optim.linear.NonNegativeConstraint;
import org.apache.commons.math3.optim.linear.Relationship;
import org.apache.commons.math3.optim.linear.SimplexSolver;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;

public class Simplexverfahren {

	public static void main(String[] args) {

		// **** Hauptbedingung entsprechend Simplex-Tableau ****
		// hier: Deckungsbeitrag optimieren (Zielfunktion):
		LinearObjectiveFunction oFunc = new LinearObjectiveFunction(new double[] { 3800, 4200, 3750, 3800 }, 0);

		// **** Nebenbedingungen entsprechend Simplex-Tableau ****
		// Kapazitaeten der Maschinen/ Fertigungen in Abhaengigkeit der Maschinenstunden
		// und der (gesuchten) Mengen der Produkte
		Collection<LinearConstraint> constraints = new ArrayList<LinearConstraint>();
		constraints.add(new LinearConstraint(new double[] { 2, 3, 2, 3 }, Relationship.LEQ, 4000));// Fertigung I
		constraints.add(new LinearConstraint(new double[] { 4, 3, 2, 2 }, Relationship.LEQ, 3400));// Fertigung II
		constraints.add(new LinearConstraint(new double[] { 3, 3, 2, 3 }, Relationship.LEQ, 3600));// Fertigung III

		// **** Loesung des Optimierungsproblems ****
		// ** Parameter **
		// MaxIter: Anzahl der maximalen Iterationen
		// oFunc: Hauptbedingung (Zielfunktion)
		// LinearConstraintSet: Nebenbedingungen
		// GoalType: Optimierung der Zielfunktion nach Maximum oder Minimum
		// NonNegativeConstraint: Wertbeschraenkung fuer Koordinaten (hier: gesuchte
		// Mengen sollen >0 sein)
		SimplexSolver solver = new SimplexSolver();
		PointValuePair solution = solver.optimize(new MaxIter(100), oFunc, new LinearConstraintSet(constraints),
				GoalType.MAXIMIZE, new NonNegativeConstraint(true));

		// **** Ausgabe der Loesung ****
		System.out.println(Arrays.toString(solution.getPoint()) + " : " + solution.getSecond());
		System.out.println("optimale Mengen: " + Arrays.toString(solution.getPoint()));
		System.out.println("optimaler Deckungsbeitrag: " + solution.getSecond());

	}

}