
public class PersonTest {

	public static void main(String[] args) {
		
		Adresse a1 = new Adresse("Mausstr", "12345", 8);
		
		Person p1;// Deklaration (Objekt ankuendigen), aber Objekt existiert noch nicht
		p1 = new Person();// Instanziierung (Objekt erzeugen)
		// Person() ist ein Konstruktor; in Klasse 'Person' noch nicht erstellt
		// -> Java geht davon aus, dass man Objekte einer Klasse instanziieren will
		//    und erstellt deshalb automatisch einen leeren Konstruktor
		p1.setName("max");
		System.out.println(p1.getName());
		p1.setAlter(20);
		System.out.println(p1.getAlter()); 
		
		Person p2 = new Person();
		System.out.println(p2.getName());
		System.out.println(p2.getAlter());

		Person p3 = new Person("Anna", 23, a1);
		System.out.println(p3.getName());
		System.out.println(p3.getAlter());
		
		System.out.println(p3.toString());
		System.out.println(p3);
		// wenn Compiler ein Objekt ausgeben soll (print() oder println()),
		// ruft er toString() auf, wenn nichts anderes gefordert wurde
		// Grund: Compiler erkennt, dass ein String gefordert wird
		
		System.out.println("--------------------------------");
		
		Person p4 = new Person("Max", 18, a1);
		Person p5 = new Person("Max", 18, a1);
		Person p6 = null;
		
		System.out.println(Person.getAnzahl());
		System.out.println(p3.getAnzahl());
		System.out.println(p6);
		
		String str = new String("bla");
		
		if(p4 == p5)// Objektreferenzen werden verglichen, nicht die Inhalte !!!
			System.out.println("gleich");
		else
			System.out.println("ungleich");
		
		if(p4.equals(p5))// Inhalte, d.h. Attribute, vergleichen
			System.out.println("gleich");
		else
			System.out.println("ungleich");
		
		System.out.println("--------------------------------");
		
		Person otto = new Person("Otto", 21, a1);
		Person kaiUwe = new Person("Kai-Uwe", 20, new Adresse("Nelkenweg", "98693", 9));
		
		System.out.println(otto);
		System.out.println(kaiUwe);
	}

}
