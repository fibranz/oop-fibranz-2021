
public class Person {

	// **** Attribute ****

	private String name;// Objektattribut
	// private: Attribut nur innerhalb der Klasse sichtbar
	// public: Attribut von aussen sichtbar/ verwendbar
	// -> Sichbarkeits-Schluesselworte (disabilities)
	// OOP: Attribute einer Klasse sollen fast immer 'private' sein
	// -> um auf diese zuzugreifen: public-Methode als Schnittstelle erstellen
	private int alter;// Objektattribut
	private static int anzahl;// Klassenattribut -> gilt fuer alle Objekte der Klasse

	private Adresse adr;// Objektattribut
	// -> Aggregation, d.h. die Klasse Person benutzt die Klasse Adresse,
	//    aber die Klasse Adresse kann auch ohne die Klasse Person existieren
	// (Komposition: "Unterklasse" kann nicht existieren, wenn "Oberklasse" nicht mehr existiert)

	// **** Konstruktoren ****
	// zur Instanziierung; Unterschied zu Methoden:
	// - haben keinen Rueckgabetyp (nichtmal 'void')
	// - heissen genau wie die Klasse selbst

	public Person() {
		this.name = "Unbekannt";
		this.alter = 0;
		this.adr = null;
		anzahl++;
	}

	// |
	// | Ueberladen/ Ueberschreiben des Konstruktors
	// \|/
	// '
	public Person(String name, int alter, Adresse adr) {
		// nicht so schoen, weil keine Kontrolle, was Nutzer eingibt:
//		this.name = name;
//		this.alter = alter;
		// besser:
		setName(name);
		setAlter(alter);
		this.adr = adr;
		anzahl++;
	}

	// **** Methoden ****
	// Schnittstelle "Aussenwelt" <-> Attribute

	// ** Klassenmethoden **

	public static int getAnzahl() {
		return anzahl;
	}

	// ** Objektmethoden **

	public String getName() {// 'getter'
		return name;
	}

	public void setName(String name) {// 'setter'
		// 'name' kann auch anders heissen, z.B. 'neuerName'
		// -> aber Konvention: gleiche Bezeichnung wie Attribut
		// -> Unterscheidung durch 'this'
		this.name = name;
	}

	public int getAlter() {
		return alter;
	}

	public void setAlter(int alter) {
		if (alter > 0)
			this.alter = alter;
	}

	public String toString() {// soll ausgeben: [Name: <name>, Alter: <alter>]
		return "[Name: " + this.name + ", Alter: " + this.alter + ", Adresse: " + this.adr + "]";
	}

	public boolean equals(Object obj) {
		// Argument: Objekt 'obj' der Oberklasse Object
		// Signatur fuer die Methode 'equals' ist so vorgegeben !!!

		// **** "Ob zwei Objekte gleich sind, entscheidet der Entwickler." ****
		// -> wird hier implementiert

//		if (obj == null)
//			// pruefen, ob das Objekt leer ist, also eine Adresse, die auf nichts zeigt ('null')
//			return false;

		if (obj instanceof Person) {// Ist 'obj' ein Objekt der Klasse Person?
			Person p = (Person) obj;// Objekt der Klasse Person
			if (this.name.equals(p.getName()) && this.alter == p.getAlter())
				// this.name ist ein Objekt der Klasse String; fuer dieses Objekt
				// wird die (fuer die Klasse String schon implementierte) Methode
				// 'equals' aufgerufen
				// this.alter ist ein primitiver Datentyp (int), fuer die ein
				// Vergleich mittels ==-Operator zulaessig ist
				return true;
			else
				return false;
		}
		return false;

	}
}
