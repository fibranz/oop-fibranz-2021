
public class Adresse {
	private String str;
	private String plz;
	private int hnr;
	
	public Adresse(String str, String plz, int hnr) {
		this.str = str;
		this.plz = plz;
		this.hnr = hnr;
	}
	public String getStr() {
		return this.str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	public String getPlz() {
		return this.plz;
	}
	public void setPlz(String plz) {
		this.plz = plz;
	}
	public int getHnr() {
		return this.hnr;
	}
	public void setHnr(int hnr) {
		this.hnr = hnr;
	}
	@Override
	public String toString() {
		return this.str + " " + this.hnr + ",  " + this.plz;
	}
	
}
