
public class ElementNichtVorhandenException extends Exception {

	// Attribute
	private int fpos;

	// Konstruktoren
	public ElementNichtVorhandenException(String msg, int fpos) {
		super(msg);// Konstruktor der Klasse Exception aufrufen
		this.fpos = fpos;
	}

	// Methoden
	public int getFpos() {
		return this.fpos;
	}

}
