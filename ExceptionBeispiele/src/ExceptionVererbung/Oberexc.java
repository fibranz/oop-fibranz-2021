package ExceptionVererbung;

public class Oberexc extends Exception {

	// Attribute
	private int einWert;

	// Konstruktoren
	public Oberexc(int w) {
		einWert = w;
	}

	// Methoden
	@Override
	public String toString() {
		return new String("einWert=" + einWert);
	}

}
