package ExceptionVererbung;

public class Unterexc extends Oberexc {

	// Attribute
	private int zweiterWert;

	// Konstruktoren
	public Unterexc(int w, int zw) {
		super(w);
		zweiterWert = zw;
	}

	// Methoden
	@Override
	public String toString() {
		return new String(super.toString() + "; zweiterWert=" + zweiterWert);
	}

}
