package ExceptionVererbung;

public class ExceptionTest {

	static void eineAusnahmeWerfen() throws Unterexc {
		throw new Unterexc(1, 2);
	}

	public static void main(String[] args) {

		System.out.println("Erste Ausnahme:");
		try {
			eineAusnahmeWerfen();// Unterexc wird in dieser Methode mittels Wurf instanziiert
		} catch (Unterexc e) {
			System.out.println(e);// toString von Unterexc wird aufgerufen: einWert=1; zweiterWert=2
		}

		System.out.println("Zweite Ausnahme:");
		try {
			eineAusnahmeWerfen();// Unterexc wird in dieser Methode mittels Wurf instanziiert
		} catch (Oberexc e) {
			System.out.println(e);// Objektmethoden werden ueberschrieben, d.h. gleiche Ausgabe wie oben
		}

	}

}
