
public class AusnahmeBeispiele2 {

	private int[] zListe = { 1, 2, 3 };

	public static void main(String[] args) {
		AusnahmeBeispiele2 ab = new AusnahmeBeispiele2();
		try {
			System.out.println("Element: " + ab.liesElem(5));
		} catch (ElementNichtVorhandenException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getFpos());
		}
	}

	public int liesElem(int pos) throws ElementNichtVorhandenException {
		if (pos >= zListe.length)
			throw new ElementNichtVorhandenException("FEHLER: unzulaessiger Index", pos);
		/*
		 * Objekt der Exception erstellen und auswerfen;
		 * 
		 * Wer diese Methode aufruft, muss den Wurf auffangen, d.h. die Ausnahme mittels
		 * try-catch behandeln. Alternativ kann die Ausnahme mittels 'throws <name>' in
		 * der Signatur an die naechsthoehere Ebende weitergeworfen werden, die sich
		 * dann um die Behandlung kuemmern muss ...
		 * 
		 * Methode wird beim Auswerfen direkt abgebrochen, d.h. nachfolgender Code wird
		 * ignoriert!
		 * 
		 */
		int erg = zListe[pos];
		return erg;
	}

}
