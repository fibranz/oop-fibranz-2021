package myUtil;

public class IntArrayTest {

	public static void main(String[] args) {

		MyIntArray feld = new MyIntArray();

		try {
			System.out.println(feld.get(3));
			feld.set(0, 2);
			feld.set(1, 4);
			feld.set(2, 6);
			feld.set(3, 8);
			feld.increase(5);
			feld.set(16, 11);
			System.out.println(feld.get(11));
		} catch (MyIndexException e) {
			System.out.println(e.getMessage() + " Fehler an Stelle " + e.getWrongIndex());
		}

	}

}
