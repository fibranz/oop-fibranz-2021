package myUtil;

public class MyIntArray implements IIntArray {

	// Attribute

	private int[] intArray;

	// Konstruktoren

	public MyIntArray() {
		this.intArray = new int[10];
	}

	// Methoden

	public int get(int index) throws MyIndexException {
		if (index < 0 || index >= intArray.length)
			throw new MyIndexException(index);
		return intArray[index];
	}

	public void set(int index, int value) throws MyIndexException {
		if (index < 0 || index >= intArray.length)
			throw new MyIndexException(index);
		this.intArray[index] = value;
	}

	public void increase(int n) {
		int[] temp = new int[intArray.length + n];
		for (int i = 0; i < intArray.length; i++)
			temp[i] = intArray[i];
		intArray = temp;
	}

}
