
public class AusnahmeBeispiele {

	public static void main(String[] args) {
		
		/*
		 * **** Ausnahmen-/ Fehlerbehandlung mit try-catch ****
		 * 
		 * zuerst Unterklassen/ spezielle Faelle abfangen, dann erst Oberklassen/
		 * allgemeinere Faelle!
		 * 
		 * beachte Verdeckung (Klassenmethoden, Objekt- & Klassenattribute) bzw.
		 * Ueberschreibung (Objektmethoden)
		 * 
		 * alle Exceptions (z.B. ArrayIndexOutOfBounds) erben von der Klasse Exception,
		 * welche von Throwable erbt und diese von der obersten Klasse Object
		 */
		
		/*
		 * **** Argumente fuer die main in Eclipse setzen ****
		 * (Angabe erfolgt eigentlich beim Aufruf der main in der Kommandozeile)
		 * 
		 * Run (kl. Pfeil nach unten) -> Run Configurations (Fenster oeffnet sich)
		 * -> linker Bereich: Doppelklick auf "Java Application" (zum Aktualisieren)
		 * -> re. Bereich: Browse Project, Search Main Class ...
		 * -> re. Bereich: Reiter Arguments: gewuenschte Argumente unter "Program Arguments" 
		 *    eintragen (mehrere durch Leerzeichen getrennt)
		 * 
		 */

		try {
			int erg = Integer.parseInt(args[0]) + Integer.parseInt(args[1]);
			System.out.println(erg);
		} catch (ArrayIndexOutOfBoundsException ex) {
			System.out.println("Error: Element nicht vorhanden");
		} catch (java.lang.NumberFormatException e) {
			System.out.println("Error: Konvertierung von String in Int nicht moeglich");
		} catch (Exception exc) {// uebrige Ausnahmen
			exc.printStackTrace();// unnoetig, weil JVM dies automatisch macht
			System.out.println("Error: ...");
		}

	}
}
