
public class ZweidimMatrixprodukt {

	public static void main(String[] args) {

		int[][] m1 = { { 1, 2 }, { 0, 1 }, { -1, -1 }, { 0, 0 } };// 4x2-Matrix
		int[][] m2 = { { 1, 2, 3 }, { 1, 2, 3 } };// 2x3-Matrix

		for (int[] zeile : m1) {
			for (int elem : zeile)
				System.out.printf("%3d ", elem);
			System.out.println();
		}

		for (int[] zeile : m2) {
			for (int elem : zeile)
				System.out.printf("%3d ", elem);
			System.out.println();
		}

		System.out.println("Matrixprodukt:");
		try {
			int[][] produkt = multipliziereMatrizen(m1, m2);
			for (int[] zeile : produkt) {
				for (int elem : zeile)
					System.out.printf("%3d", elem);
				System.out.println();
			}
		} catch (MatrixproduktNichtMoeglichException e) {
			System.out.println(e.getMessage());
		}

	}

	public static int[][] multipliziereMatrizen(int[][] matrixA, int[][] matrixB)
			throws MatrixproduktNichtMoeglichException {
		if (matrixA[0].length != matrixB.length)
			throw new MatrixproduktNichtMoeglichException(
					"Fehler: Matrixmultiplikation aufgrund verschiedener Dimensionen unzulaessig");
		int[][] produktAB = new int[matrixA.length][matrixB[0].length];
		int elemAB = 0;
		for (int m = 0; m < produktAB.length; m++) {
			for (int n = 0; n < produktAB[0].length; n++) {
				for (int k = 0; k < matrixA[0].length; k++)
					elemAB += matrixA[m][k] * matrixB[k][n];
				produktAB[m][n] = elemAB;
				elemAB = 0;
			}
		}
		return produktAB;
	}

}
