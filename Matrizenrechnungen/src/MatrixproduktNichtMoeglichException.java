
public class MatrixproduktNichtMoeglichException extends Exception {

	public MatrixproduktNichtMoeglichException(String msg) {
		super(msg);
	}

}
