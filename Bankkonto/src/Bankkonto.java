
public class Bankkonto {

	// Attribute
	private String kontoinhaber;
	private String kontonummer;
	private double kontostand;
	
	// Konstruktoren
	public Bankkonto(String kontoinhaber, String kontonummer, double kontostand) {
		this.kontoinhaber = kontoinhaber;
		this.kontonummer = kontonummer;
		setKontostand(kontostand);
	}

	// Methoden
	public double getKontostand() {
		return kontostand;
	}

	public void setKontostand(double kontostand) {
		this.kontostand = kontostand;
	}

	public void einzahlen(double betrag) {
		kontostand += betrag;
	}

	public double auszahlen(double betrag) {
		kontostand -= betrag;
		return betrag;
	}

	@Override
	public String toString() {
		return "Kontoinhaber: " + kontoinhaber + ", Kontonummer: " + kontonummer + ", Kontostand: " + kontostand + "EUR";
	}

}
