
public class BankkontoTest {

	public static void main(String[] args) {
		double stand = 60.0;
		double auszahlwunsch = 60.0;
		double dispo = 100.0;

		Bankkonto[] konten = new Bankkonto[4];
		konten[0] = new Bankkonto("Urs Hui", "1234", stand);
		konten[1] = new Bankkonto("Ursl Hoi", "5678", stand);
		konten[2] = new Dispokonto("Urs Hui", "1234", stand, dispo);
		konten[3] = new Dispokonto("Ursl Hoi", "5678", stand, dispo);

		Bankkonto kto1 = new Bankkonto("Julia Mueller", "0123 4567 0000 8910", stand);
		System.out.println(kto1);
		System.out.println("Stand: " + kto1.getKontostand());
		System.out.println("Auszahlung: " + kto1.auszahlen(auszahlwunsch));
		System.out.println("Stand: " + kto1.getKontostand());
		System.out.println("Auszahlung: " + kto1.auszahlen(auszahlwunsch));
		System.out.println("Stand: " + kto1.getKontostand());
		System.out.println("Auszahlung: " + kto1.auszahlen(auszahlwunsch));
		System.out.println("Stand: " + kto1.getKontostand());
		System.out.println("Auszahlung: " + kto1.auszahlen(auszahlwunsch));
		System.out.println("Stand: " + kto1.getKontostand());

		System.out.println("------------------");

		Dispokonto dKto1 = new Dispokonto("Julia Mueller", "0123 4567 0000 8910", stand, dispo);
		System.out.println(dKto1);
		System.out.println("Stand: " + dKto1.getKontostand());
		System.out.println("Wunsch: " + auszahlwunsch + ", Auszahlung: " + dKto1.auszahlen(auszahlwunsch));
		System.out.println("Stand: " + dKto1.getKontostand());
		System.out.println("Wunsch: " + auszahlwunsch + ", Auszahlung: " + dKto1.auszahlen(auszahlwunsch));
		System.out.println("Stand: " + dKto1.getKontostand());
		System.out.println("Wunsch: " + auszahlwunsch + ", Auszahlung: " + dKto1.auszahlen(auszahlwunsch));
		System.out.println("Stand: " + dKto1.getKontostand());
		System.out.println("Wunsch: " + auszahlwunsch + ", Auszahlung: " + dKto1.auszahlen(auszahlwunsch));
		System.out.println("Stand: " + dKto1.getKontostand());
		System.out.println(dKto1);

		for (int i = 0; i < konten.length; i++)
			System.out.println(konten[i]);

	}

}
