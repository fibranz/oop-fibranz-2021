
public class Dispokonto extends Bankkonto{
	
	// Attribute
	double dispokredit;
	
	// Konstruktoren
	public Dispokonto(String kontoinhaber, String kontonummer, double kontostand, double dispokredit) {
		super(kontoinhaber, kontonummer, kontostand);
		setDispokredit(dispokredit);
	}

	// Methoden
	public double getDispokredit() {
		return dispokredit;
	}
	
	public void setDispokredit(double dispokredit) {
		this.dispokredit = dispokredit;
	}
	@Override
	public double auszahlen(double betrag) {
		double ktoStand = getKontostand();
		if((ktoStand - betrag) > -dispokredit) {
			setKontostand(ktoStand - betrag);
			return betrag;
		}
		else {
			betrag = dispokredit + ktoStand;
			setKontostand(-dispokredit);
			return betrag;
		}			
	}
	@Override
	public String toString() {
		return super.toString() + ", Dispokredit: " + dispokredit + "EUR";
	}

}
