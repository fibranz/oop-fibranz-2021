import java.util.ArrayList;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Iterator;
// Alternative: import java.util.*;// importiert alles aus dem Paket java.util (-> siehe API)
// bei wenigen zu importierenden Komponenten diese besser explizit importieren, nicht mit *

public class ContainerKlassenBeispiele {

	public static void main(String[] args) {

		// **** Array: stat. Datenstruktur / Laenge fest ****

		System.out.println("**** Array ****");
		String[] sListe = new String[2];
		sListe[0] = "str1";
		sListe[1] = "str2";
//		sListe[2] = "str3";// FEHLER
		System.out.println(sListe.length);

		System.out.println("---------------------------------");

		// **** ArrayList: dynam. Datenstruktur / Laenge veraenderbar ****

		System.out.println("**** ArrayList ****");
		ArrayList<String> strListe = new ArrayList<String>();
		ArrayList<String> tListe = new ArrayList<String>();
		strListe.add("str1");
		strListe.add("str2");
		strListe.add("str2");
		strListe.add("str3");
		strListe.remove("str2");// alternativ: strListe.remove(1)
		tListe.add("str2");
		tListe.add("str5");
		strListe.addAll(tListe);

		System.out.println("  ** ArrayList auf einmal ausgeben (toString) **");
		System.out.println(strListe);// moeglich, weil toString implementiert ist

		strListe.removeAll(tListe);// alles entfernen, was in tListe drin ist

		System.out.println("  ** ArrayList vorwaerts durchlaufen - 3 Moeglichkeiten **");

		for (int i = 0; i < strListe.size(); i++)
			System.out.print(strListe.get(i) + " ");
		System.out.println();
		for (String value : strListe)
			System.out.print(value + " ");
		System.out.println();
		System.out.println("Groesse: " + strListe.size());
		for (Iterator<String> it = strListe.iterator(); it.hasNext();)
			System.out.print(it.next() + " ");
		System.out.println();

		System.out.println("  ** ArrayList rueckwaerts durchlaufen (ListIterator) **");
		int anzahl = strListe.size();
		ListIterator<String> it = strListe.listIterator(anzahl);
		while (it.hasPrevious())
			System.out.print(it.previous() + " ");

		System.out.println("\n---------------------------------");

		System.out.println("**** TreeSet ****");
		TreeSet<String> treeListe = new TreeSet<String>();
		treeListe.add("Guten");
		treeListe.add("Gruezi");
		treeListe.add("Morgen");
		treeListe.add("!");

		System.out.println("  ** TreeSet auf einmal ausgeben (toString) **");
		System.out.println(treeListe);

		System.out.println("  ** TreeSet vorwaerts durchlaufen - 2 Moeglichkeiten **");
		for (String s : treeListe)
			System.out.print(s + " ");
		System.out.println();
		for (Iterator<String> itTree = treeListe.iterator(); itTree.hasNext();)
			// itTree: Iterator fuer treeListe
			System.out.print(itTree.next() + " ");
		// Reihenfolge der Ausgabe basiert auf compareTo (String implementiert
		// Comparable)

		System.out.println("\n---------------------------------");

		System.out.println("**** HashSet ****");
		HashSet<String> hashListe = new HashSet<String>();
		hashListe.add("Guten");
		hashListe.add("Gruezi");
		hashListe.add("Morgen");
		hashListe.add("!");

		System.out.println("  ** HashSet auf einmal ausgeben (toString) **");
		System.out.println(hashListe);

		System.out.println("  ** HashSet vorwaerts durchlaufen - 2 Moeglichkeiten **");
		for (String s : hashListe)
			System.out.print(s + " ");
		System.out.println();
		for (Iterator<String> itHash = hashListe.iterator(); itHash.hasNext();)
			// hashTree: Iterator fuer hashListe
			System.out.print(itHash.next() + " ");
		// Reihenfolge der Ausgabe basiert auf Hashcode der Elemente

	}
}
