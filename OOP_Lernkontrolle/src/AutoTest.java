
public class AutoTest {

	public static void main(String[] args) {

		Auto auto1 = new Auto("Blau", 123.0);
		System.out.println(auto1);

		System.out.println("---------------");

//		System.out.println(auto1.getGeschwindigkeit());
		System.out.println("Bremsen von " + auto1.bremse() + " auf 0.0 km/h");
//		System.out.println(auto1.getGeschwindigkeit());

		Auto auto2 = new Auto();
		System.out.println(auto2);
		System.out.println("Bremsen von " + auto2.bremse() + " auf 0.0 km/h");

	}

}
