
public class Auto {

	// Attribute

	private String farbe;
	private double geschwindigkeit;// in km/h

	// Konstruktoren

	public Auto() {
		this.farbe = "Weiß";
		this.geschwindigkeit = 0.0;
	}

	public Auto(String farbe, double geschwindigkeit) {
		setFarbe(farbe);
		this.geschwindigkeit = geschwindigkeit;
	}

	// Methoden

	public String getFarbe() {
		return this.farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

//	public double getGeschwindigkeit() {
//		return this.geschwindigkeit;
//	}
//
//	public void setGeschwindigkeit(double geschwindigkeit) {
//		this.geschwindigkeit = geschwindigkeit;
//	}

	public double bremse() {
		double tempo = this.geschwindigkeit;
		this.geschwindigkeit = 0.0;
		return tempo;
	}

	@Override
	public String toString() {
		return "Auto-Informationen:\n" + "\tFarbe: " + this.farbe + "\n\tGeschwindigkeit: " + this.geschwindigkeit
				+ " km/h";
	}

}
