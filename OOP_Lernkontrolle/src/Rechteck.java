
public class Rechteck {

	// Attribute

	private int a;
	private int b;

	// Konstruktor

	public Rechteck(int a, int b) {
		this.a = a;
		this.b = b;
	}

	// Methoden

	public int getA() {
		return a;
	}

	public int getB() {
		return b;
	}

	public boolean isUnit() {
		return this.a == 1 && this.b == 1;
	}

}
