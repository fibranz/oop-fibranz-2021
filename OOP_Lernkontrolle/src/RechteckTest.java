
public class RechteckTest {

	public static void main(String[] args) {

		Rechteck rechteck1 = new Rechteck(1, 1);
		System.out.println("Rechteck (Oberklasse):");
		System.out.println(rechteck1.getA());
		System.out.println(rechteck1.getB());
		System.out.println(rechteck1.isUnit());

		System.out.println("Quader (Unterklasse):");
		Quader quader1 = new Quader(2, 3, 4);
		System.out.println(quader1.getA());
		System.out.println(quader1.getB());
		System.out.println(quader1.getC());
		System.out.println(quader1.isUnit());

	}

}
