
public class Quader extends Rechteck {

	// Attribute

	private int c;// 3. Seite

	// Konstruktor

	public Quader(int a, int b, int c) {
		super(a, b);
		this.c = c;
	}

	// Methoden

	public int getC() {
		return this.c;
	}

	@Override
	public boolean isUnit() {
		return super.isUnit() && this.c == 1;
	}

}
