
public class Ticket {
	// Klasse zur Verwaltung der Informationen eines Tickets

	// **** Attribute ****

	private String bezeichner;
	private double preis;
	private int anzahl;

	// **** Konstruktoren ****

	public Ticket() {
		this.bezeichner = null;
		this.preis = 0.0;
		this.anzahl = 0;
	}

	public Ticket(String bezeichner, double preis, int anzahl) {
		setBezeichner(bezeichner);
		setPreis(preis);
		setAnzahl(anzahl);
	}

	// **** Methoden

	public void setBezeichner(String bezeichner) {
		if (bezeichner instanceof String)
			this.bezeichner = bezeichner;
	}

	public void setPreis(double preis) {
		if (preis >= 0)
			this.preis = preis;
	}

	public void setAnzahl(int anzahl) {
		if (anzahl >= 0)
			this.anzahl = anzahl;
	}
	
	public String getBezeichner() {
		return this.bezeichner;
	}
	
	public double getPreis() {
		return this.preis;
	}
	
	public int getAnzahl() {
		return this.anzahl;
	}
	
	@Override
	public String toString() {
		return this.anzahl + "x " + this.bezeichner + " a " + this.preis + "EUR";
	}

}
