
public class File {

	// Attribute
	private String filename;
	private Time creatingTime;

	// Konstruktoren

	public File() {
		creatingTime = new Time();
		filename = null;
	}

	public File(String name, Time time) throws TimeException {
//		creatingTime = new Time(h, m, s);
		creatingTime = time;
		setFilename(name);
		setTime(time);
	}

	// Methoden

	public String getFilename() {
		return filename;
	}

	public void setFilename(String name) {
		filename = name;
	}

	public Time getTime() {
		return creatingTime;
	}

	public void setTime(Time time) {
		creatingTime = time;
	}

}
