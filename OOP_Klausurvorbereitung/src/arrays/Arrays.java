package arrays;

import java.util.ArrayList;
import java.util.Iterator;

public class Arrays {

	// **** Methode fuer Array ****
	public static int wieOftEnthaltenArray(int[] array, int value) {
		int zaehler = 0;
		for (int elem : array) {
			if (elem == value)
				zaehler++;
		}
		return zaehler;
	}

	// **** Methode fuer ArrayList ****
	public static int wieOftEnthalten(ArrayList<Integer> array, Integer value) {
		int zaehler = 0;
		for (Iterator<Integer> it = array.iterator(); it.hasNext();) {
			if (it.next() == value)
				zaehler++;
		}
		return zaehler;
	}

	public static void main(String[] args) {

		// **** Array ****
		int[] intArray1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		int wert1 = 2;
		System.out.printf("%d: %dx%n", wert1, wieOftEnthaltenArray(intArray1, wert1));

		// **** ArrayList ****
		Integer wert2 = 1;
		Integer wert3 = wert1;
//		Integer wert4 = Integer(wert1);// FEHLER
		ArrayList<Integer> intListe1 = new ArrayList<Integer>();
		intListe1.add(0);
		intListe1.add(0);
		intListe1.add(0);
		intListe1.add(0);
		intListe1.add(1);
		intListe1.add(1);
		intListe1.add(1);
		intListe1.add(2);
		intListe1.add(2);
		intListe1.add(3);
		System.out.printf("%d: %dx%n", wert2, wieOftEnthalten(intListe1, wert2));
		System.out.printf("%d: %dx%n", wert3, wieOftEnthalten(intListe1, wert3));

	}
}
