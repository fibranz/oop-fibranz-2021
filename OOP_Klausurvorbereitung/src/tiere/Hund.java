package tiere;

public class Hund extends Tier {

	// Attribute
	private static String klasse;// Klassenattribut

	// Konstruktor
	public Hund(String name, int alter) {
		super(name, alter);
		klasse = "Hund";
	}

	// Methoden
	public String getKlasse() {
		return klasse;
	}

	public String geraeusch() {
		return "wuff";
	}

	@Override
	public String toString() {
		return super.toString() + " (" + klasse + ")";
	}

}
