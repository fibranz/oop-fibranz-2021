package tiere;

public abstract class Tier {

	private String eigenname;
	private int alter;

	// Methode, die das Tiergeraeusch als String zurueckgibt
	public abstract String geraeusch();

	public Tier(String eigenname, int alter) {
		this.eigenname = eigenname;
		this.alter = alter;
	}

	public String toString() {
		return "Name: " + eigenname + ", Alter: " + alter;
	}

}
