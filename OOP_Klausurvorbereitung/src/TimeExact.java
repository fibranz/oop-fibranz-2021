
public class TimeExact extends Time {

	// Attribute

	private int tenthSeconds;
	private int hundredthSeconds;

	// Konstruktoren

	public TimeExact() {
		super();
		tenthSeconds = 0;
		hundredthSeconds = 0;
	}

	public TimeExact(int h, int m, int s, int ts, int hs) throws TimeException {
		super(h, m, s);
		if (ts < 0 || ts > 9)
			throw new TimeException("WrongTime");
		if (hs < 0 || hs > 9)
			throw new TimeException("WrongTime");
		tenthSeconds = ts;
		hundredthSeconds = hs;
	}

	// Methoden

	@Override
	public String toString() {
		String[] time = super.toString().split(" ");
		// String von time wird am Leerzeichen in 2 Teile gespalten: time[0] und time[1]
		String exactSeconds = String.format(",%d%d", tenthSeconds, hundredthSeconds);
		return time[0] + exactSeconds + " " + time[1];
	}

}
