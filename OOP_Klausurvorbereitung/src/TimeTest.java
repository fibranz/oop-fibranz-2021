
public class TimeTest {

	public static void main(String[] args) {

		Time time1 = new Time();
		Time time2 = new Time();
		Time time3 = new Time();
		TimeExact timeExact1 = new TimeExact();
		Time[] timeArray = new Time[3];

		try {
			time2 = new Time(19, 40, 36);
			timeExact1 = new TimeExact(19, 40, 36, 8, 4);
			timeArray[0] = time1;
			timeArray[1] = time2;
			timeArray[2] = timeExact1;

		} catch (TimeException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("---------------------------");
		try {
			System.out.println(time1);
			System.out.println(time2);
			System.out.println(timeExact1);
			System.out.println(new TimeExact(19, 40, 36, 9, 4));

			System.out.println(time1.compareTo(new Time()));
			System.out.println(time1.compareTo(time2));
			System.out.println(time2.compareTo(time1));

			for (Time value : timeArray)
				System.out.print(value + "  ");
		} catch (TimeException e) {
			System.out.println(e.getMessage());

		}
		System.out.println("\n---------------------------");
		try {
			time3 = new Time(23, 59, 59);
			System.out.println(time3);
			timeExact1 = new TimeExact(23, 59, 59, 9, 9);
			System.out.println(timeExact1);
		} catch (TimeException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("---------------------------");
		try {
			File file1 = new File();
			System.out.println(file1.getFilename());
			System.out.println(file1.getTime());
			file1 = new File("javacode.java", new Time(12, 2, 2));
			System.out.println(file1.getFilename() + " " + file1.getTime());
			System.out.println(file1.getTime());
		} catch (TimeException e) {
			System.out.println(e.getMessage());
		}

	}
}
