
public class Time implements Comparable<Time> {

	// Attribute

	private int hours;
	private int minutes;
	private int seconds;

	// Konstruktoren

	public Time() {
		hours = 0;
		minutes = 0;
		seconds = 0;
	}

	public Time(int h, int m, int s) throws TimeException {
		if ((h < 0) || (h > 23))
			throw new TimeException("WrongTime");
		if ((m < 0) || (m > 59))
			throw new TimeException("WrongTime");
		if ((s < 0) || (s > 59))
			throw new TimeException("WrongTime");
		seconds = s;
		minutes = m;
		hours = h;
	}

	// Methoden

	public int getHours() {
		return hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	@Override
	public String toString() {
		String uhrzeit = String.format("%02d:%02d:%02d Uhr", hours, minutes, seconds);
		return uhrzeit;
	}

	@Override
	public int compareTo(Time o) {
		int thisInSeconds = this.getHours() * 60 * 60 + this.getMinutes() * 60 + this.getSeconds();
		int oInSeconds = o.getHours() * 60 * 60 + o.getMinutes() * 60 + o.getSeconds();
		return thisInSeconds - oInSeconds;
	}

}
