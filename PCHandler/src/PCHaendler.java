import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// **** Benutzereingaben lesen ****

//		System.out.println("was m�chten Sie bestellen?");
//		String artikel = myScanner.next();
//
//		System.out.println("Geben Sie die Anzahl ein:");
//		int anzahl = myScanner.nextInt();
//
//		System.out.println("Geben Sie den Nettopreis ein:");
//		double preis = myScanner.nextDouble();
//
//		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
//		double mwst = myScanner.nextDouble();

		String artikel = liesString("Was m�chten Sie bestellen? ");// Artikel
		int anzahl = liesInt("Geben Sie die Anzahl ein: ");// Anzahl
		double preis = liesDouble("Geben Sie den Nettopreis ein: ");// Einzelpreis netto
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein: ");// MwSt

		// **** Verarbeiten ****

//		double nettogesamtpreis = anzahl * preis;
//		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// **** Ausgeben ****

//		System.out.println("\tRechnung");
//		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
//		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

		rechnungAusgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}

	public static String liesString(String text) {
		System.out.print(text);
		Scanner stringScanner = new Scanner(System.in);
		String textIN = stringScanner.next();
		stringScanner.nextLine();
		return textIN;
	}

	public static int liesInt(String text) {
		System.out.print(text);
		Scanner intScanner = new Scanner(System.in);
		int intIN = intScanner.nextInt();
		intScanner.nextLine();
		return intIN;
	}

	public static double liesDouble(String text) {
		System.out.print(text);
		Scanner doubleScanner = new Scanner(System.in);
		double doubleIN = doubleScanner.nextDouble();
		doubleScanner.nextLine();
		return doubleIN;
	}

	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double gesamtNetto = anzahl * nettopreis;
		return gesamtNetto;
	}

	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double MwSt) {
		double gesamtBrutto = nettogesamtpreis * (1 + MwSt / 100.0);
		return gesamtBrutto;
	}

	public static void rechnungAusgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		System.out.println("************\n Rechnung\n************");
		System.out.printf("\t Netto:  %-20s %6d %10.2f EUR%n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t Brutto: %-20s %6d %10.2f EUR (%.1f%%)%n", artikel, anzahl, bruttogesamtpreis, mwst);
	}
}
