import java.util.Scanner;

public class NameAlter {

	public static void main(String[] args) {
  
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Nachname?");
		String nachname = myScanner.next();
		myScanner.nextLine();// Eingabestrom loeschen, wie in C nach scanf: fflush(stdin);
		System.out.println("Vorname?");
		String vorname = myScanner.next();
		myScanner.nextLine();// Eingabestrom loeschen, wie in C nach scanf: fflush(stdin);
		System.out.println("Alter?");
		int alter = myScanner.nextInt();
		myScanner.nextLine();// Eingabestrom loeschen, wie in C nach scanf: fflush(stdin);

		System.out.printf("%-12s%-20s%n", "Vorname:", vorname);
		System.out.printf("%-12s%-20s%n", "Nachname:", nachname);
		System.out.printf("%-12s%-20d%n", "Alter:", alter);

		myScanner.close();

	}

}
