
public class Hello {

	public static void main(String[] args) {

		int iZahl = 123;

//		System.out.println("Hello ...");

//		System.out.println(iZahl);

//		System.out.printf("%d", iZahl);
//		System.out.printf("%5d%n", iZahl);// 5-stelliger Bereich; rechtsbuendig
//		System.out.printf("%-5d%n", iZahl);// 5-stelliger Bereich; linksbuendig
//		System.out.printf("%+-5d%n", iZahl);// '+': VZ sichtbar
//		System.out.printf("%+-5d\n", -iZahl);// '+': VZ sichtbar
//		System.out.printf("%07d%n", iZahl);// 5-stelliger Bereich mit 0 aufgefuellt
//		System.out.printf("%5d --- %5d%n", iZahl, 456);

//		double fZahl = 1234.56789;
//		String fFormat = "|%010.3f|%n";// Variable
//		fFormat = "|%010.1f|%n";// Variable veraendern
//		final String FFORMAT = "|%010.3f|%n";// Konstante
////		FFORMAT = "|%010.1f|%n";// FALSCH - Konstanten sind unveraenderlich!
//
//		System.out.printf("|%f|%n", fZahl);
//		System.out.printf("|%.2f|%n", fZahl);
//		System.out.printf("|%10.2f|%n", fZahl);
//		System.out.printf("|%-10.2f|%n", fZahl);
//		System.out.printf("|%010.2f|%n", fZahl);
//		System.out.printf(fFormat, fZahl);
//		System.out.printf(FFORMAT, fZahl);

		String str = "123456789";

		System.out.printf("|%s|%n", str);
		System.out.printf("|%.4s|%n", str);
		System.out.printf("|%20s|%n", str);
		System.out.printf("|%-20s|%n", str);
		System.out.printf("|%-20.4s|%n", str);// nur die ersten 4 Zeichen

	}

}
