import java.util.Scanner;// Klasse "Scanner" importieren

public class EingabeBeispiele {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		// Objekt erstellen mit der Eigenschaft "Standardeingabe"

		System.out.println("Geben Sie eine Zahl ein!");
		int zahl = myScanner.nextInt();
		myScanner.nextLine();// Eingabestrom loeschen, wie in C nach scanf: fflush(stdin);
		// in Kommandozeile klicken und Zahl eingeben

//		int zahl2 = myScanner.nextInt();
//		float kommazahl = myScanner.nextFloat();
		System.out.println("was eingeben:");
		String str1 = myScanner.next();
//		String str2 = myScanner.nextLine();

		System.out.println("zahl = " + zahl);// "zahl = ..."
		System.out.println("zahl = " + zahl);// "zahl = ..."
//		System.out.println(2+"1");// String-Konkatenation

		System.out.println(str1);// "zahl = ..."

		myScanner.close();

		int meineZahl = 2;

		// Kontrollstrukturen in C: if-else, switch-case, while, do-while, for
		// Kontrollstrukturen in Java: if-else, ... (spaeter mehr)

		if (meineZahl >= 0) {
			System.out.println("Zahl positiv oder Null");
		} else {
			System.out.println("Zahl negativ");
		}

	}

}
