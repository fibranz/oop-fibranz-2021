
public class Buch implements Comparable<Buch> {

	private String autor;
	private String titel;
	private String isbn;

	public Buch(String autor, String titel, String isbn) {
		super();
		setAutor(autor);
		setTitel(titel);
		setIsbn(isbn);
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Buch) {
			Buch buch2 = (Buch) obj;
			if (isbn.compareTo(buch2.getIsbn()) == 0)
				return true;
		}
		return false;
	}

	// Sortieren von Objekten der Klasse Buch ermoeglichen:
	@Override
	public int compareTo(Buch o) {
		return getIsbn().compareTo(o.getIsbn());
		// Alternativen:
		// return isbn.compareTo(o.getIsbn());
		// return this.isbn.compareTo(o.getIsbn());
		// return this.getIsbn().compareTo(o.getIsbn());
	}

	@Override
	public String toString() {
		return "[" + autor + ", " + titel + ", " + isbn + "]";
	}

}
