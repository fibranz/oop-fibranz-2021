import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class CollectionTest {

	// **** selbst implementiert ****
//	public static Buch sucheIsbn(List<Buch> buecherliste, String isbn) {
//		for (Iterator<Buch> it = buecherliste.iterator(); it.hasNext();) {
//			Buch b = it.next();
//			if (b.getIsbn().equals(isbn))// equals-Methode fuer Strings
//				return b;
//		}
//		return null;
//	}
	// **** moeglichst viele Methoden von Collections ****
	public static Buch sucheIsbn(List<Buch> buecherliste, String isbn) {
		Collections.sort(buecherliste);// Liste muss fuer binarySearch sortiert sein!
		Buch b = new Buch("irgend", "was", isbn);
		// autor und titel beliebig, weil die Sortierung auf compareTo basiert
		// und compareTo lediglich auf der isbn
		int index = Collections.binarySearch(buecherliste, b);
//		System.out.println("TEST: index=" + index);
		if (index >= 0)
			return buecherliste.get(index);
		return null;
	}

	// **** selbst implementiert ****
//	public static String entferneBuch(List<Buch> buecherliste, Buch buchZuLoeschen) {
//		for (Iterator<Buch> it = buecherliste.iterator(); it.hasNext();) {
//			Buch b = it.next();
//			if (b.equals(buchZuLoeschen)) {// equals-Methode fuer Objekte der Klasse Buch
//				buecherliste.remove(buchZuLoeschen);
//				return String.format("%s erfolgreich geloescht.", buchZuLoeschen);
//			}
//		}
//		return String.format("%s ist nicht vorhanden.", buchZuLoeschen);
//	}
	// **** moeglichst viele Methoden von Collections ****
	public static String entferneBuch(List<Buch> buecherliste, Buch buchZuLoeschen) {
		Collections.sort(buecherliste);// Liste muss fuer binarySearch sortiert sein!
		int index = Collections.binarySearch(buecherliste, buchZuLoeschen);
		if (index >= 0) {
			buecherliste.remove(index);
			return String.format("%s erfolgreich geloescht.", buchZuLoeschen);
		}
		return String.format("%s ist nicht vorhanden.", buchZuLoeschen);
	}

	// **** selbst implementiert ****
//	public static String sucheGroessteIsbn(List<Buch> buecherliste) {
//		String groessteIsbn = buecherliste.get(0).getIsbn();
//		for(Iterator<Buch> it = buecherliste.iterator(); it.hasNext(); ) {
//			Buch b = it.next();
//			if(b.getIsbn().compareTo(groessteIsbn) > 0)
//				groessteIsbn = b.getIsbn();
//		}
//		return groessteIsbn;
//	}
	// **** moeglichst viele Methoden von Collections ****
	public static String sucheGroessteIsbn(List<Buch> buecherliste) {
		return Collections.max(buecherliste).getIsbn();
	}

	// **** Hauptprogramm ****
	public static void main(String[] args) {

		// **** Buecherkisten erzeugen ****

		System.out.println("**** LinkedList-Buecherkisten, gespeichert als List (Interface) ****");
		List<Buch> buecherkiste1 = new LinkedList<Buch>();
		Buch buch1 = new Buch("grobi", "oop", "0123");
		Buch buch2 = new Buch("tier", "oop SS21", "8888");
		buecherkiste1.add(buch1);
		buecherkiste1.add(new Buch("liese", "java", "4567"));
		buecherkiste1.add(new Buch("bert", "oop mit java", "8910"));
		buecherkiste1.add(new Buch("erni", "oop mit python", "2345"));
		List<Buch> buecherkiste2 = new ArrayList<Buch>(buecherkiste1);// buecherkiste1 kopieren

		// **** Test der Funktionen an beiden Buecherkisten ****

		System.out.println("  ** Funktionentest an Kiste 1 **");
		System.out.println(buecherkiste1);
		System.out.println(sucheIsbn(buecherkiste1, "4567"));
		Collections.sort(buecherkiste1);
		System.out.println(buecherkiste1);
		System.out.println(entferneBuch(buecherkiste1, buch1));
		System.out.println(entferneBuch(buecherkiste1, buch2));
		System.out.println(buecherkiste1);
		System.out.println(sucheGroessteIsbn(buecherkiste1));

		System.out.println("  ** Funktionentest an Kiste 2 **");
		System.out.println(buecherkiste2);
		System.out.println(sucheIsbn(buecherkiste2, "2345"));
		Collections.sort(buecherkiste2);
		System.out.println(buecherkiste2);
		System.out.println(entferneBuch(buecherkiste2, buecherkiste2.get(buecherkiste2.size() - 1)));
		System.out.println(entferneBuch(buecherkiste2, buch2));
		System.out.println(buecherkiste2);
		System.out.println(sucheGroessteIsbn(buecherkiste2));

		System.out.println("----------------------------------------------------------------");

		System.out.println("**** TreeSet-Buecherkiste, gespeichert als Set (Interface) ****");
		Set<Buch> treeListe = new TreeSet<Buch>();
		treeListe.add(buch1);
		treeListe.add(buch2);
		treeListe.add(new Buch("liese", "java", "4567"));
		treeListe.add(new Buch("erni", "oop mit python", "2345"));
		for (Iterator<Buch> it = treeListe.iterator(); it.hasNext();)
			System.out.println(it.next());
		System.out.println(treeListe);

	}
}
