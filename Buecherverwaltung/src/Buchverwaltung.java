import java.util.*;

public class Buchverwaltung {

	// **** Klassenattribute ****

	private static List<Buch> buchliste = new ArrayList<Buch>();// ArrayList implementiert List
	private static Scanner myScanner = new Scanner(System.in);

	// **** Methoden ****

	static void menue() {
		System.out.println("\n ***** Buchverwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) loeschen");
		System.out.println(" 4) Die hoechste ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 6) sortieren");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {

//		List<Buch> buchliste = new ArrayList<Buch>();
//		Scanner myScanner = new Scanner(System.in);

		// Buchliste initialisieren:
		buchliste.add(new Buch("grobi", "oop", "0123"));
		buchliste.add(new Buch("liese", "java", "4567"));
		buchliste.add(new Buch("bert", "oop mit java", "8910"));
		buchliste.add(new Buch("erni", "oop mit python", "2345"));

		char wahl;
		String eintrag;
		int index;

		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':
				buchEintragen();
				break;
			case '2':
				buchFinden();
				break;
			case '3':
				buchLoeschen();
				break;
			case '4':
				buchMitHoechsterIsbnZeigen();
				break;
			case '5':
				buchlisteZeigen();
				break;
			case '6':
				buchlisteSortieren();
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
			}// switch

		} while (wahl != 9);
	}// main

	public static void buchEintragen() {
		System.out.println("Neues Buch eintragen");
		System.out.println("Autor:");
		String autor = myScanner.next();
		System.out.println("Titel:");
		String titel = myScanner.next();
		System.out.println("ISBN:");
		String isbn = myScanner.next();
		Buch b = new Buch(autor, titel, isbn);
		buchliste.add(b);
	}

	public static void buchFinden() {
		System.out.println("Titel zu ISBN finden");
		System.out.println("ISBN:");
		boolean istEnthalten = false;
		String isbn = myScanner.next();
		for (Buch buch : buchliste) {
			if (buch.getIsbn().equals(isbn)) {
				System.out.println("Titel: " + buch.getTitel());
				istEnthalten = true;
				break;
			}
		}
		if (istEnthalten == false)
			System.out.println("Das Buch ist nicht vorhanden.");
	}

	public static void buchLoeschen() {
		System.out.println("Titel zu ISBN loeschen");
		System.out.println("ISBN:");
		String isbn = myScanner.next();
		Buch b = null;
		for (Buch buch : buchliste) {
			if (buch.getIsbn().equals(isbn)) {
				b = buch;
				break;// gefunden
			}
		}
		if (b != null)
			buchliste.remove(b);
		else
			System.out.println("Buch ist nicht vorhanden.");
	}

	public static void buchMitHoechsterIsbnZeigen() {
		Collections.sort(buchliste);
		System.out.println(buchliste.get(buchliste.size() - 1));
	}

	public static void buchlisteSortieren() {
		Collections.sort(buchliste);
		// Sortierung basiert auf compareTo, d.h. Sortierung hier nach ISBN
	}

	public static void buchlisteZeigen() {
		System.out.println(buchliste);
	}

}
