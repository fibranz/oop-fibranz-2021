import java.util.Comparator;

public class MyCompImp implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		return o1.compareTo(o2) * (-1);// String-Sortierung in umgekehrter Reihenfolge
	}

}
