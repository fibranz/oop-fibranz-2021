
public class Person implements Comparable<Person> {

	private int pNr;
	private String name;

	public Person(int pNr, String name) {
		super();
		this.pNr = pNr;
		this.name = name;
	}

	public int getpNr() {
		return pNr;
	}

	public void setpNr(int pNr) {
		this.pNr = pNr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Person o) {
		return pNr - o.getpNr();
	}

	@Override
	public String toString() {
		return "Person [pNr=" + pNr + ", name=" + name + "]";
	}

}
