import java.util.*;

public class ArrayListBeispiele {

	public static void main(String[] args) {

		List<String> list = new ArrayList<>();
		list.add("Freistädter");
		list.add("Zipfer");
		list.add("Schnaitl");
		list.add("Brauhaus Gusswerk");

		Collections.sort(list);// Sortierung nach compareTo() der Klasse String
		System.out.println(list);

		// **** Comparator<T> (Interface) ****
		//
		// Ist eine andere Sortierung gewuenscht, muss sort() mit 2 Parametern
		// aufgerufen werden (2. Parameter ist ein sog. Comparator)
		//
		// ** Sinn eines Comparator (Interface) **
		// ermoeglicht andere Sortierprinzipien durch
		// Ueberschreiben von compare() unter Beruecksichtigung des
		// Prinzips der Flexibilitaet/ Wiederverwendbarkeit!
		//
		// ** Achtung **
		// Implementierung von Comparable<T> -> compareTo() definieren
		// Implemtenierung von Comparator<T> -> compare() definieren

		// ** 1. Moeglichkeit **
		// eigene Klasse erstellen, die Comparator implementiert (nur compareTo())
		// (nur zum Verstaendnis, macht so keiner)
//        MyCompImp mc = new MyCompImp();        
//        Collections.sort(list, mc);

		// ** 2. Moeglichkeit **
		// anonyme Klasse (Klasse ohne Namen) als 2. Parameter definieren
		Collections.sort(list, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2) * -1;
			}
		});
		// Anonyme Klassen werden v.a. in der GUI-Programmierung verwendet.
		// Vorteil: Es muss keine Klasse erstellt werden
		// (-> Nachteil, wenn die gleiche anonyme Klasse oft gebraucht wird)

		System.out.println(list);
	}

}
