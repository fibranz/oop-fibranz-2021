import java.util.*;

public class PersonTest {

	public static void main(String[] args) {

		ArrayList<Person> list = new ArrayList<Person>();
		list.add(new Person(1001, "name5"));
		list.add(new Person(1002, "name4"));
		list.add(new Person(1003, "name3"));
		list.add(new Person(1004, "name2"));
		list.add(new Person(1005, "name1"));

		Collections.sort(list);// Sortierung nach compareTo() der Klasse Person (-> pNr)
		System.out.println(list);

		// andere Sortierung mittels anonymer Klasse, die Comparator implementiert:
		Collections.sort(list, new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getName().compareTo(o2.getName());// Sortierung nach compareTo der Klasse String
			}
		});
		System.out.println(list);

	}
}
