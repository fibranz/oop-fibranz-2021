import java.util.Scanner;

public class MinZahl {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("1. Zahl:");
		int zahl1 = myScanner.nextInt();
		myScanner.nextLine();

		System.out.println("2. Zahl:");
		int zahl2 = myScanner.nextInt();
		myScanner.nextLine();

		if (zahl1 < zahl2) {
			System.out.println("Minimum: " + zahl1);
		} else {
			System.out.println("Minimum: " + zahl2);
		}
		
		myScanner.close();

	}

}
