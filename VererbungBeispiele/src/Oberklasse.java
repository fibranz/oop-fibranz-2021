
public class Oberklasse {

	public int objAttr = 1;
	public static int klAttr = 2;

	public void objektmethode() {
		System.out.println("Oberklasse: objektmethode");
	}

	public static void klassenmethode() {
		System.out.println("Oberklasse: klassenmethode");
	}

	@Override
	public String toString() {
		return "Oberklasse: toString -> " + objAttr + " | ";
	}

}
