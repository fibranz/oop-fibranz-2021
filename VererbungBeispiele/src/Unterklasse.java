
public class Unterklasse extends Oberklasse {

	private int zahl = 8;
	public double objAttr = 100.0;
	public static int klAttr = 200;

	// Objektmethode 'objektmethode' ueberschreiben:
	public void objektmethode() {
		System.out.println("Unterklasse: objektmethode");
	}

	// Klassenmethode 'klassenmethode' wird ueberdeckt
	public static void klassenmethode() {
		System.out.println("Unterklasse: klassenmethode");
	}

	// Objektmethode 'objektmethode' ueberschreiben:
	@Override
	public String toString() {
		return "Unterklasse: toString -> " + objAttr + " | " + zahl;
	}

}
