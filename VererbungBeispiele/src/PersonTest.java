
public class PersonTest {

	public static void main(String[] args) {

		Person p1 = new Person();
		System.out.println(p1);

		Teilnehmer t1 = new Teilnehmer();
		System.out.println(t1);

		Person[] pListe = new Person[4];
		pListe[0] = p1;
		pListe[1] = t1;
		pListe[2] = new Person("Max", 18);
		pListe[3] = new Teilnehmer("Anna", 23, "ledig");
		for (int i = 0; i < pListe.length; i++)
			System.out.println(pListe[i]);

		System.out.println("-------------------");
		for (Person mensch : pListe) {
			System.out.println(mensch);
			System.out.println((mensch.getName().equals("Anna")) ? (mensch.getName()) : (mensch.getAlter()));
		}

	}

}
