
public class VererbungTest {

	public static void main(String[] args) {

		Unterklasse uk = new Unterklasse();

		System.out.println(uk);
		System.out.println((Oberklasse) uk);
		System.out.println("------------------");
		uk.objektmethode();
		((Oberklasse) uk).objektmethode();
		// gibt trotzdem 'objektmethode' der Unterklasse aus, weil sie bei der
		// Instanziierung ueberschrieben wurde, d.h. das gecastete Objekt
		// verwendet die ueberschriebene Methode (alte Methode existiert nicht mehr)
		System.out.println("Fazit: Objektmethoden werden ueberschrieben, d.h. alte existieren dann nicht mehr.");
		System.out.println("------------------");
		uk.klassenmethode();
		((Oberklasse) uk).klassenmethode();
		// gibt 'klassenmethode' der Oberklasse aus, weil sie bei der
		// Instanziierung nur verdeckt wurde, d.h. das gecastete Objekt
		// verwendet die alte (noch existierende) Methode
		System.out.println("Fazit: Klassenmethoden werden verdeckt, d.h. alte existieren noch.");
		System.out.println("------------------");
		System.out.println(uk.objAttr);
		System.out.println(((Oberklasse) uk).objAttr);
		System.out.println(uk.klAttr);
		System.out.println(((Oberklasse) uk).klAttr);
		System.out.println("Fazit: Objekt- und Klassenattribute werden verdeckt, d.h. alte existieren noch.");

	}

}
