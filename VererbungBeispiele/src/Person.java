
public class Person {

	protected String name;
	private int alter;

	private static int anzahl = 0;

	public Person() {
		this.name = "Unbekannt";
		this.alter = 0;
		anzahl++;
	}

	public Person(String name, int alter) {
		setName(name);
		setAlter(alter);
		anzahl++;
	}

	public static int getAnzahl() {
		return anzahl;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAlter() {
		return alter;
	}

	public void setAlter(int alter) {
		if (alter > 0)
			this.alter = alter;
	}

	@Override
	public String toString() { // [ Name: Max , Alter: 18 ]
		return "Name: " + this.name + ", Alter: " + this.alter;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Person) {
			Person p = (Person) obj;
			if (this.name.equals(p.getName()) && this.alter == p.getAlter())
				return true;
			else
				return false;
		}
		return false;
	}

}
