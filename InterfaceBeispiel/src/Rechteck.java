
public class Rechteck implements Form{
	
	// Attribute

	private double a;
	private double b;

	// Konstruktoren

	public Rechteck(double a, double b) {
		this.a = a;
		this.b = b;
	}

	// Methoden

	@Override
	public double berechneFlaeche() {
		return this.a * this.b;
	}

	@Override
	public double berechneUmfang() {
		return 2 * this.a + 2 * this.b;
	}

	@Override
	public String toString() {
		return "Rechteck (a = " + this.a + ", b = " + this.b + ")";
	}


}
