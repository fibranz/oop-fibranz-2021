
public class Kreis implements Form {

	// Attribute

	private double radius;

	// Konstruktoren

	public Kreis(double radius) {
		this.radius = radius;
	}

	// Methoden

	@Override
	public double berechneFlaeche() {
		return this.radius * this.radius * PI;
	}

	@Override
	public double berechneUmfang() {
		return 2 * PI * this.radius;
	}

	@Override
	public String toString() {
		return "Kreis (r = " + this.radius + ")";
	}

}
