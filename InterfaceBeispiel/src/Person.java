
public class Person implements Comparable<Person> {
	// Klasse 'Person' implementiert die Schnittstelle 'Comparable'
	// fuer Instanzen der Klasse 'Person'

	private int personalnummer;
	private String name;

	public Person(String name, int personalnummer) {
		setName(name);
		setPersonalnummer(personalnummer);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPersonalnummer() {
		return personalnummer;
	}

	public void setPersonalnummer(int personalnummer) {
		if (personalnummer > 0)
			this.personalnummer = personalnummer;
	}

	public String toString() { // [ Name: Max , Alter: 18 ]
		return "[ Name: " + this.name + " , Alter: " + this.personalnummer + " ]";
	}

	// Sortierung nach Personalnummer ermoeglichen:
//	@Override
//	public int compareTo(Person p) {
////		if(this.personalnummer > p.getPersonalnummer())
////			return 1;
////		if(this.personalnummer < p.getPersonalnummer())
////			return -1;
////		return 0;
//		// Alternative:
//		return this.personalnummer - p.getPersonalnummer();
//	}

	// Sortierung nach Name ermoeglichen:
	@Override
	public int compareTo(Person p) {// Methode zum Vergleichen von Objekten der Klasse Person

		return this.name.compareTo(p.getName());
		/*
		 * hier wird 'compareTo' der Klasse String aufgerufen (die Klasse String
		 * beinhaltet bereits eine Implementierung von 'compareTo'), weil das
		 * Vergleichskriterium fuer Objekte der Klasse 'Person' in dieser Methode der
		 * Name, also ein String, sein soll; es wird also innerhalb der fuer 'Person' zu
		 * implementierenden Methode 'compareTo' eine andere Methode aufgerufen, die
		 * auch 'compareTo' heisst, allerdings fuer ein anderes Objekt gilt, naemlich
		 * ein String (das ist KEIN rekursiver Aufruf!)
		 * 
		 * Anmerkung: vermutlich werden die Strings buchstabenweise entsprechend der
		 * ASCII-Tabelle verglichen (evtl. ueber Differenzen der den Buchstaben
		 * entsprechenden Zahlen)
		 */
	}

}
