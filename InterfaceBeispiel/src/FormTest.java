
public class FormTest {

	public static void main(String[] args) {

		Kreis k1 = new Kreis(3.0);
		System.out.println(k1);
		System.out.printf(" Flaeche: %.3f%n", k1.berechneFlaeche());
		System.out.printf(" Umfang: %.3f%n", k1.berechneUmfang());

		Rechteck q1 = new Rechteck(3.0, 2.0);
		System.out.println(q1);
		System.out.printf(" Flaeche: %.3f%n", q1.berechneFlaeche());
		System.out.printf(" Umfang: %.3f%n", q1.berechneUmfang());
		
		System.out.println("-----------------------");
		
		Form k2 = new Kreis(2.0);
		Form q2 = new Rechteck(2.0, 2.0);
		System.out.println(k2);
		System.out.println(q2);

		Form[] formen = new Form[2];
		formen[0] = k1;
		formen[1] = q1;
		for(Form form : formen)
			System.out.println(form);

	}

}
