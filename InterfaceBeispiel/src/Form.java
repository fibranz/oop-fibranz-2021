
public interface Form {// Schnittstelle

	// Attribute (sind automatisch immer 'public' und 'final' (Konstanten))

	double PI = 3.14159;

	// Methoden (sind automatisch immer 'public')

	double berechneFlaeche();

	double berechneUmfang();

}
