
public class PersonTest {

	public static void main(String[] args) {

		Person p1 = new Person("Max", 123);
		Person p2 = new Person("Anna", 126);

		System.out.println(p1.compareTo(p2));
		System.out.println(p2.compareTo(p1));
		System.out.println(p2.compareTo(new Person("Paul", 126)));

		Person[] pListe = new Person[2];
		pListe[0] = p1;
		pListe[1] = p2;
		for (Person value : pListe)
			System.out.print(value + " ");

	}

}
