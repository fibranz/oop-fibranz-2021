
public class GeoObjectTest {

	public static void main(String[] args) {

		Rechteck r1 = new Rechteck(1, 1, 3, 4);
		System.out.println(r1);
		System.out.printf(" Flaeche = %.3f%n", r1.determineArea());

		Kreis k1 = new Kreis(-1, -1, 2);
		System.out.println(k1);
		System.out.printf(" Flaeche = %.3f%n", k1.determineArea());

		GeoObject obj1 = new Rechteck(0, 0, 3, 4);
		System.out.println(obj1 + " | Flaeche = " + obj1.determineArea());

	}

}
