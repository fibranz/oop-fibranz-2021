
public class Rechteck extends GeoObject {

	// Attribute

	private double hoehe;
	private double breite;

	// Konstruktoren

	public Rechteck(double x, double y, double hoehe, double breite) {
		super(x, y);
		this.hoehe = hoehe;
		this.breite = breite;
	}

	// Methoden

	@Override
	public double determineArea() {
		return this.hoehe * this.breite;
	}

	@Override
	public String toString() {
		return super.toString() + "; Rechteck (Hoehe = " + this.hoehe + ", Breite = " + this.breite + ")";
	}

}
