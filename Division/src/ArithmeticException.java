
public class ArithmeticException extends Exception {

	// Konstruktor
	public ArithmeticException(String msg) {
		super(msg);
	}

}
