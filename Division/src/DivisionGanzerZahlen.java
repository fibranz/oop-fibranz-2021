
public class DivisionGanzerZahlen {

	public static void main(String[] args) {

		try {
			System.out.println(divisionGanzerZahlen(4, 2));
			System.out.println(divisionGanzerZahlen(4, 0));
		} catch (ArithmeticException e) {
			System.out.println(e.getMessage());
		}

	}

	public static int divisionGanzerZahlen(int dividend, int divisor) throws ArithmeticException {
		if (divisor == 0)
			throw new ArithmeticException("MATH ERROR: Division durch Null.");
		return dividend / divisor;
	}

}
