import java.util.Scanner;

public class Grosshaendler01 {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Anzahl der bestellten PC-Maeuse:");
		int anzahl = myScanner.nextInt();
		myScanner.nextLine();
		System.out.println("Preis einer PC-Maus:");
		float preis = myScanner.nextFloat();
		myScanner.nextLine();

		if (anzahl >= 10) {
			System.out.println("Kostenfreier Versand.");
			System.out.printf("Betrag (incl. 19%% MwSt.): %.2fEUR%n", anzahl * 1.19 * preis);
		} else {
			System.out.println("Es fallen Versandgeb�hren in Hoehe von 10,-� an.");
			System.out.printf("Betrag (incl. 19%% MwSt.): %.2fEUR%n", anzahl * 1.19 * preis + 10);
		}

		myScanner.close();
	}

}
