import java.util.Scanner;

public class Grosshaendler02 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Anzahl der bestellten PC-Maeuse:");
		int anzahl = myScanner.nextInt();
		myScanner.nextLine();
		System.out.println("Preis einer PC-Maus:");
		float preis = myScanner.nextFloat();
		myScanner.nextLine();
		
//		float festpreis = 1.80f;// bei double kein angehaengtes 'f'
		float betrag = anzahl*preis;
		
		if(betrag < 100) {
			betrag *= 0.9;// 10% Rabatt
			System.out.println("10% Rabatt auf den Netto-Preis.");
		}else if(betrag >= 100 && betrag < 500) {
			betrag *= 0.85;// 15% Rabatt
			System.out.println("15% Rabatt auf den Netto-Preis.");
		}else {
			betrag *= 0.8;// 20% Rabatt
			System.out.println("20% Rabatt auf den Netto-Preis.");
		}

		if (anzahl >= 10) {
			System.out.println("Kostenfreier Versand.");
			System.out.printf("Betrag (incl. 19%% MwSt.): %.2fEUR%n", 1.19*betrag);
		} else {
			System.out.println("Es fallen Versandgeb�hren in Hoehe von 10,-EUR an.");
			System.out.printf("Betrag (incl. 19%% MwSt.): %.2fEUR%n", 1.19*betrag + 10);
		}

		myScanner.close();

	}

}
