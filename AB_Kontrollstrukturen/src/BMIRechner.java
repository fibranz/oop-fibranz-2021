import java.util.Scanner;

public class BMIRechner {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

//		// **** TEST ****
//		double masse = 68.0;
//		double groesse = 170.0;
//		String geschlecht = "W";

		System.out.println("Masse [kg]?");
		double masse = myScanner.nextDouble();
		myScanner.nextLine();
		System.out.println("Groesse [cm]?");
		double groesse = myScanner.nextDouble();
		myScanner.nextLine();
		System.out.println("Geschlecht [m/w]?");
		char geschlecht = myScanner.next().charAt(0);
		myScanner.nextLine();

		double bmi = masse / (groesse * (1e-2) * groesse * (1e-2));
		// BMI berechnen (mit Groesse in m)

		if (geschlecht == 'm' || geschlecht == 'M') {
			// wenn 'geschlecht' vom Typ String: geschlecht.equals("m")...
			if (bmi < 20) {
				System.out.printf("BMI = %.1f -> Untergewicht", bmi);
			} else if (bmi > 25) {
				System.out.printf("BMI = %.1f -> Uebergewicht", bmi);
			} else {
				System.out.printf("BMI = %.1f -> Normalgewicht", bmi);
			}
		} else if (geschlecht == 'w' || geschlecht == 'W') {
			// wenn 'geschlecht' vom Typ String: geschlecht.equals("w")...
			if (bmi < 19) {
				System.out.printf("BMI = %.1f -> Untergewicht", bmi);
			} else if (bmi > 24) {
				System.out.printf("BMI = %.1f -> Uebergewicht", bmi);
			} else {
				System.out.printf("BMI = %.1f -> Normalgewicht", bmi);
			}
		} else {
			System.out.printf("BMI = %.1f", bmi);
		}

		myScanner.close();
	}

}
