import java.util.Scanner;

public class Schleifenbeispiele {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

//		// **** Demo-Beispiele ****
//
//		int x = 5;
//
//		// ** kopfgesteuerte Schleifen **
//		
//		while (x > 2) {
//			System.out.println("x: " + x);
//			x--;
//		}
//
//		for (int i = 0; i < 10; i++) {
//			System.out.print(i + " ");
//		}
//		System.out.println();
//		for (int i = 9; i >= 0; i--) {
//			System.out.print(i + " ");
//		}
//		System.out.println();
//		for (int i = 0; i < 10; i++) {
//			System.out.print((9-i) + " ");
//		}
//
//		System.out.println("-------------------------");
//		x = 5;
//
//		// ** fussgesteuerte Schleifen **  
//		
//		do {
//			System.out.println("x: " + x);
//			x--;
//		} while (x > 2);
//
//		System.out.println("-------------------------");
//
//		// **** Aufgabenblatt 2 ****
//
//		// ** Aufg. 1a: Zaehlen **
//		
//		System.out.print("Bis zu welcher nat�rlichen Zahl soll gez�hlt werden? ");
//		int natZahl = myScanner.nextInt();
//		myScanner.nextLine();
//		int k = 1;
//		do {
//			System.out.println(k);
//			k++;
//		} while (k <= natZahl);
//
//		System.out.println("-------------------------");
//
//		// ** Aufg. 3: Quersumme **
//
//		System.out.print("Zahl, deren Quersumme berechnet werden soll? ");
//		int querZahl = myScanner.nextInt();
//		myScanner.nextLine();
//
//		int quersumme = 0;
//		int zehnerpotenz = 10;
//		System.out.printf("Die Quersumme von %d ist ", querZahl);
//		do {
//			quersumme += querZahl % zehnerpotenz;
////			System.out.println("TEST: " + quersumme);
//			querZahl /= zehnerpotenz;
//		} while (Math.abs(querZahl) >= 1);
//		System.out.printf("%d.%n", quersumme);
//		
//		System.out.println("-------------------------");

		// **** Aufgabenblatt 3 ****

		// ** Aufg. 2c: Summe **

		System.out.print("Obere Grenze der Summenfolge: ");
		int grenze = myScanner.nextInt();
		myScanner.nextLine();
		int summe = 0;
		for (int i = 1; i <= grenze; i += 2) {
			summe += i;
//			System.out.printf("TEST: %d ", i);
		}
		System.out.printf("%nSumme: %d %n", summe);

		// ** Aufg. 6: Sterne **

		System.out.print("Anzahl der Pyramidenstufen: ");
		grenze = myScanner.nextInt();
		myScanner.nextLine();
		int m, n = 1;
		for (m = 1; m <= grenze; m++) {
			for (n = 1; n <= m; n++) {
				System.out.printf("%-1s", "*");
			}
			System.out.println();
		}

		char stern = '*';
		String sternZeile = "";
		for (m = 1; m <= grenze; m++) {
			sternZeile += stern;
			System.out.printf("%s", sternZeile);
			System.out.print("\n");

		}

	}

}
