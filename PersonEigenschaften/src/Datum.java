
public class Datum {

	// **** Attribute ****

	private int tag, monat, jahr;

	// **** Konstruktoren ****

	public Datum() {
		tag = 1;
		monat = 1;
		jahr = 1970;
	}

	public Datum(int tag, int monat, int jahr) {
		setDatum(tag, monat, jahr);
	}

	// **** Methoden ****

	public void setDatum(int tag, int monat, int jahr) {
		if (tag > 0 && tag <= 31)
			this.tag = tag;
		if (monat > 0 && monat <= 12)
			this.monat = monat;
		if (jahr > 0)
			this.jahr = jahr;
	}

	public int getTag() {
		return tag;
	}

	public int getMonat() {
		return monat;
	}

	public int getJahr() {
		return jahr;
	}

	public String toString() {
		return tag + "." + monat + "." + jahr;
	}

	public boolean equals(Object obj) {
		if (obj instanceof Datum) {
			Datum d = (Datum) obj;
			boolean istTagGleich = (this.tag == d.getTag());
			boolean istMonatGleich = (this.monat == d.getMonat());
			boolean istJahrGleich = (this.jahr == d.getJahr());
			if (istTagGleich && istMonatGleich && istJahrGleich)
				return true;
			else
				return false;
		}
		return false;
	}

	public static int berechneQuartal(Datum d) {
		int monat = d.getMonat();
		if (monat >= 1 && monat <= 3)
			return 1;
		if (monat >= 4 && monat <= 6)
			return 2;
		if (monat >= 7 && monat <= 9)
			return 3;
		if (monat >= 10 && monat <= 12)
			return 4;
		return 0;// Fehlerfall
	}

}
