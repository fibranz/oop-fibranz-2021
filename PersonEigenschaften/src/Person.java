
public class Person {
	// **** Attribute ****

	private String name;
	private Datum gebDatum;
	private int personID;// nicht-statisch
	private static int anzahl;// statisch

	// **** Konstruktoren ****

	public Person() {
		this.name = "unbekannt";
		this.gebDatum = new Datum(0, 0, 0);
		anzahl++;
		this.personID = anzahl;
	}

	public Person(String name, int tag, int monat, int jahr) {
		setName(name);
		setGebDatum(tag, monat, jahr);
		anzahl++;
		this.personID = anzahl;
	}

	// **** Methoden ****

	// ** Klassenmethoden **

	public static int getPersonenanzahl() {
		return anzahl;
	}

	// ** Objektmethoden **

	public void setName(String name) {
		this.name = name;
	}

	public void setGebDatum(int tag, int monat, int jahr) {
		this.gebDatum = new Datum(tag, monat, jahr);
	}

	public void setPerson(String name, Datum gebDatum) {
		setName(name);
		setGebDatum(gebDatum.getTag(), gebDatum.getMonat(), gebDatum.getJahr());
	}

	public String getName() {
		return this.name;
	}

	public Datum getGebDatum() {
		return this.gebDatum;
	}

	public int getPersonID() {
		return this.personID;
	}

	@Override
	public String toString() {
		return "Name: " + this.name + ", Geburtsdatum: " + this.gebDatum + ", ID: " + this.personID;
	}

}
