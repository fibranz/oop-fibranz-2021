
public class PersonTest {

	public static void main(String[] args) {
		Person urs = new Person();
		System.out.println("Personenanzahl: " + Person.getPersonenanzahl());
		Person ulf = new Person("Ulf", 2, 4, 1988);
		System.out.println("Personenanzahl: " + Person.getPersonenanzahl());
		Person kalle = new Person();
		kalle.setPerson("Kalle", new Datum(23, 1, 1992));
		System.out.println("Personenanzahl: " + Person.getPersonenanzahl());

		System.out.println(urs);
		urs.setName("Urs");
		urs.setGebDatum(14, 4, 1833);
		System.out.printf("%s | %s, %s, %d%n", urs, urs.getName(), urs.getGebDatum(), urs.getPersonID());
		System.out.println(ulf);
		System.out.println(kalle);

	}

}
