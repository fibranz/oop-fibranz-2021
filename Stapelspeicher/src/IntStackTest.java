
public class IntStackTest {

	public static void main(String[] args) {

		IntStack myStack = new Stack(5);
		// **** Merke ****
		// Wo man Interfaces benutzen kann, sollte man es auch tun!
		// (d.h. anstelle "Stack" als Datentyp soll "IntStack" verwendet werden)
		//
		// ** Grund **
		// Die Implementierung der Stack-Klasse kann flexibel veraendert werden.
		//
		// ** Beispiel **
		// IntStack myStack = new Stapel(5);
		// -> 'Stapel' ist eine Klasse, die - wie auch 'Stack' - das Interface IntStack
		// implementiert.

		myStack.push(5);
		myStack.push(2);
		myStack.push(-7);
		myStack.push(3);

		System.out.println(myStack);
		try {
			System.out.println("pop: " + myStack.pop());
		} catch (StackLeerException e) {
			System.out.println(e.getMessage());
			System.out.println("Elementanzahl: " + e.getAnzahlElem());
		}
		try {
			System.out.println("pop: " + myStack.pop());
		} catch (StackLeerException e) {
			System.out.println(e.getMessage());
			System.out.println("Elementanzahl: " + e.getAnzahlElem());
		}
		System.out.println(myStack);

		myStack.push(1);
		myStack.push(2);
		myStack.push(3);
		System.out.println(myStack);
		try {
			System.out.println("pop: " + myStack.pop());
		} catch (StackLeerException e) {
			System.out.println(e.getMessage());
			System.out.println("Elementanzahl: " + e.getAnzahlElem());
		}
		System.out.println(myStack);
		myStack.push(3);
		myStack.push(4);
		System.out.println(myStack);

		for (int i = 0; i < 6; i++) {
			try {
				myStack.pop();
			} catch (StackLeerException e) {
				System.out.println(e.getMessage());
				System.out.println("Elementanzahl: " + e.getAnzahlElem());
			}
		}

		System.out.println(myStack);
		try {
			myStack.pop();
		} catch (StackLeerException e) {
			System.out.println(e.getMessage());
			System.out.println("Elementanzahl: " + e.getAnzahlElem());
		}

		myStack.push(8);
		System.out.println(myStack);

	}

}
