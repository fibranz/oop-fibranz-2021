
public class StackLeerException extends Exception {

	// Attribute
	private int anzahlElem;

	// Konstruktoren
	public StackLeerException(String msg) {
		super(msg);
		anzahlElem = 0;
	}

	// Methoden
	public int getAnzahlElem() {
		return this.anzahlElem;
	}

}
