
public interface IntStack {

	// Attribute

	// Methoden

	void push(int elem);

	int pop() throws StackLeerException;

}
