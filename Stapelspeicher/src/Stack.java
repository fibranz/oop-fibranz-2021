
public class Stack implements IntStack {

	// Attribute

	private int[] stack;
	private int firstFreeIndex;

	// Konstruktoren

	public Stack(int groesse) {
		if (groesse > 0)
			this.stack = new int[groesse];
		else
			this.stack = new int[10];
		firstFreeIndex = 0;
	}

	// Methoden

	@Override
	public void push(int elem) {
		if (firstFreeIndex >= this.stack.length) {
			// Stack voll -> Stack um 1 vergroessern
			int[] stackKopie = new int[firstFreeIndex + 1];
			for (int i = 0; i < this.stack.length; i++)
				stackKopie[i] = this.stack[i];

			this.stack = stackKopie;// this.stack soll auf neuen/ groesseren Stack verweisen
		}
		this.stack[firstFreeIndex++] = elem;
	}

	@Override
	public int pop() throws StackLeerException {
		if (firstFreeIndex <= 0)
			throw new StackLeerException("FEHLER: Der Stack ist bereits leer.");
		return this.stack[--firstFreeIndex];
	}

	@Override
	public String toString() {
		String result = "";
		for (int i = 0; i < firstFreeIndex; i++)
			result += String.format(" %d ", this.stack[i]);
		result += "\n(Stack mit " + firstFreeIndex + " Elementen)\n";
		return result;
	}

}
