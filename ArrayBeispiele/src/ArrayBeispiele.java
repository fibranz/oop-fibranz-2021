
public class ArrayBeispiele {

	public static void main(String[] args) {

		int zahl1;
		int zahl2;
		// ...
		int zahl100;

		int[] zahlen;// reine Deklaration (ohne Speicherplatz-Allokation!)
		zahlen = new int[100];// Speicherplatz wird reserviert/ allokiert
		int[] zahlen2 = new int[5];// Deklaration mit Speicherplatz-Allokation

		// Zuweisung a)
		zahlen[0] = 1;
		zahlen[1] = 2;
		zahlen[2] = 3;
		zahlen[3] = 4;
		zahlen[4] = 4;

		// Ausgabe a)
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i] + " ");
		}

		System.out.println("\n----------------------");

		// Zuweisung b)
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i + 1;
		}

		// Ausgabe b)
		for (int i = 0; i < zahlen.length; i++) {
			System.out.print(zahlen[i] + " ");
		}

		// Zuweisung und Ausgabe
		for (int i = 0; i < zahlen2.length; i++) {
			zahlen2[i] = 2 * i + 1;
			System.out.print(zahlen2[i] + " ");
		}
		System.out.println();

		// alternative Ausgabe
		for (int value : zahlen2) {
			System.out.print(value + " ");
		}

		System.out.println("\n----------------------");

		// **** weitere Arrays ****

		double[] dZahlen = new double[10];

		String[] sListe = new String[10];

		sListe[0] = "str1";// optimal
		sListe[1] = new String("str2");// ALTERNATIV (-> OOP)

		int[] zListe = { 3, 89, 5, 54, 6 };
//		// ALTERNATIV (Schreibweise wie in C):
//		int zListe[] = { 3, 89, 5, 54, 6 };
//		// nicht so schoen, weil es nicht zum Schema (typ name = wert;) passt
		int max = max(zListe);

		System.out.println("Maximum: " + max);

		System.out.println("\n----------------------");

		// **** zweidimensionale Arrays ****

		// folgende Matrix erzeugen:
		// | 1 2 3 |
		// | 4 5 6 |

		int[][] intMatrix = new int[2][3];
		// ergibt
		// | 0 0 0 |
		// | 0 0 0 |

		intMatrix[0][0] = 1;// 1. Zeile: 1 0 0
		intMatrix[0][1] = 2;// 1. Zeile: 1 2 0
		intMatrix[0][2] = 3;// 1. Zeile: 1 2 3
		intMatrix[1][0] = 4;// 2. Zeile: 4 0 0
		intMatrix[1][1] = 5;// 2. Zeile: 4 5 0
		intMatrix[1][2] = 6;// 2. Zeile: 4 5 6

		for (int zeile = 0; zeile < intMatrix.length; zeile++) {
			// length: Groesse der 1. Dimension
			for (int spalte = 0; spalte < intMatrix[0].length; spalte++)
				// [0].length: Groesse des 1. Elements der 2. Dimension
				System.out.print(intMatrix[zeile][spalte] + " ");
			System.out.println();
		}

		System.out.println("----------------------");

		int[] liste = { 1, 2, 3 };
		int[][] intMatrix2 = { liste, { 7, 8, 9 }, { 7, 8, 9 } };
		printIntMatrix(intMatrix2);

	}

	public static int max(int[] liste) {
		int m = liste[0];
		for (int i = 0; i < liste.length; i++) {
			if (m < liste[i])
				m = liste[i];
		}
		return m;

	}

	public static void printIntMatrix(int[][] zweidMatrix) {
		for (int zeile = 0; zeile < zweidMatrix.length; zeile++) {
			// length: Groesse der 1. Dimension
			for (int spalte = 0; spalte < zweidMatrix[0].length; spalte++)
				// [0].length: Groesse des 1. Elements der 2. Dimension
				System.out.print(zweidMatrix[zeile][spalte] + " ");
			System.out.println();
		}
		return;
	}

}
