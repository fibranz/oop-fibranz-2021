
public class UngeradeZahlen {

	public static void main(String[] args) {

		int[] zahlen = new int[10];

		// Array beschreiben
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = 2 * i + 1;
		}

		// Ausgabe
		for (int i : zahlen) {
			System.out.print(i + "\t");
		}

		System.out.println();

	}

}
