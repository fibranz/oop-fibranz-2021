
public class Lotto {

	public static void main(String[] args) {

//		int[] lottozahlen = new int[6];
//		lottozahlen[0] = 3;
//		lottozahlen[1] = 7;
//		lottozahlen[2] = 12;
//		lottozahlen[3] = 18;
//		lottozahlen[4] = 37;
//		lottozahlen[5] = 42;

		int[] lottozahlen = { 3, 7, 12, 18, 37, 42 };

		// Ausgabe
		intArrayAusgeben(lottozahlen);

		// Pruefen
		pruefeZahlEnthalten(12, lottozahlen);
		pruefeZahlEnthalten(13, lottozahlen);

	}

	public static void intArrayAusgeben(int[] array) {
		System.out.print("[ ");
		for (int value : array)
			System.out.print(value + " ");
		System.out.println("]");
		return;
	}

	public static boolean pruefeZahlEnthalten(int zahl, int[] array) {
		boolean istEnthalten = false;
		for (int i : array) {
			if (i == zahl) {
				istEnthalten = true;
				break;// damit Schleife nicht bis zuende durchlaufen wird
			}
		}
		if (istEnthalten == true) {
			System.out.printf("%d ist enthalten.%n", zahl);
		} else {

			System.out.printf("%d ist nicht enthalten.%n", zahl);
		}
		return istEnthalten;
	}

}
