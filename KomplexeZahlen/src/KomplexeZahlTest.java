
public class KomplexeZahlTest {

	public static void main(String[] args) {

		KomplexeZahl zahl1 = new KomplexeZahl();
		KomplexeZahl zahl2 = new KomplexeZahl(1.5, 3.2);
		KomplexeZahl zahl3 = new KomplexeZahl(1.5 + 1e-10, 3.2);
		KomplexeZahl zahl4 = new KomplexeZahl(1.5, 3.2);
		KomplexeZahl zahl5 = new KomplexeZahl(2.0, 1.0);
		KomplexeZahl zahl6 = new KomplexeZahl(4.0, 3.0);

		System.out.println(zahl1);
		System.out.println(zahl3);

		istZahlGleich(zahl1, zahl1);
		istZahlGleich(zahl1, zahl2);
		istZahlGleich(zahl2, zahl3);
		istZahlGleich(zahl2, zahl4);

		System.out.println("-------------------------");

		System.out.println(KomplexeZahl.multipliziereKomplexeZahlen(zahl5, zahl6));
		System.out.println(KomplexeZahl.multipliziereKomplexeZahlen(zahl6, zahl5));
		System.out.println(KomplexeZahl.multipliziereKomplexeZahlen(zahl1, zahl2));
		System.out.println(KomplexeZahl.multipliziereKomplexeZahlen(zahl4, zahl5));

		System.out.println(zahl5.multipliziereMitKompZahl(zahl6));
		System.out.println(zahl6.multipliziereMitKompZahl(zahl5));
		System.out.println(zahl1.multipliziereMitKompZahl(zahl2));
		System.out.println(zahl4.multipliziereMitKompZahl(zahl5));

	}

	public static void istZahlGleich(KomplexeZahl z1, KomplexeZahl z2) {
		if (z1.equals(z2))
			System.out.printf("%s == %s%n", z1, z2);
		else
			System.out.printf("%s != %s%n", z1, z2);
	}

}
