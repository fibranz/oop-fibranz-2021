
public class KomplexeZahl {
	// **** Attribute ****

	private double real, imag;

	// **** Konstruktoren ****

	public KomplexeZahl() {
		this.real = 0.0;
		this.imag = 0.0;
	}

	public KomplexeZahl(double real, double imag) {
		setKomplexeZahl(real, imag);
	}

	// **** Methoden ****

	// ** Klassenmethoden **

	public static KomplexeZahl multipliziereKomplexeZahlen(KomplexeZahl z1, KomplexeZahl z2) {
		KomplexeZahl produkt = new KomplexeZahl();
		produkt.setReal(z1.getReal() * z2.getReal() - z1.getImag() * z2.getImag());
		produkt.setImag(z1.getReal() * z2.getImag() + z1.getImag() * z2.getReal());
		return produkt;
	}

	// ** Objektmethoden **

	public void setReal(double real) {
		this.real = real;
	}

	public void setImag(double imag) {
		this.imag = imag;
	}

	public void setKomplexeZahl(double real, double imag) {
		setReal(real);
		setImag(imag);
	}

	public double getReal() {
		return this.real;
	}

	public double getImag() {
		return this.imag;
	}

	@Override // Hinweis fuer Compiler, der prueft, ob die Methode ueberhaupt existiert
	public String toString() {
		return real + " + j" + imag;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof KomplexeZahl) {
			KomplexeZahl zahl = (KomplexeZahl) obj;
			if ((this.real == zahl.getReal()) && (this.imag == zahl.imag))
				return true;
			else
				return false;
		}
		return false;
	}

	public KomplexeZahl multipliziereMitKompZahl(KomplexeZahl z) {
//		KomplexeZahl produkt = new KomplexeZahl();
//		produkt.setReal(this.real * z.getReal() - this.imag * z.getImag());
//		produkt.setImag(this.real * z.getImag() + this.imag * z.getReal());
//		return produkt;
		double a, b;
		a = this.real * z.getReal() - this.imag * z.getImag();
		b = this.real * z.getImag() + this.imag * z.getReal();
		return new KomplexeZahl(a, b);
	}

}
