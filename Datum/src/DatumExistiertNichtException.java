
public abstract class DatumExistiertNichtException extends Exception {

	// Attribute
	private int falscherWert;

	// Konstruktoren
	public DatumExistiertNichtException(String msg, int falscherWert) {
		super(msg);
		this.falscherWert = falscherWert;
	}

	// Methoden
	public int getFalscherWert() {
		return this.falscherWert;
	}

}
