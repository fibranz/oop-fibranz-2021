
public class DatumTest {
	public static void main(String[] args) {

		int day = 22, month = 5, year = 2021;

		Datum date1 = new Datum();
		Datum date2;
		Datum date3 = new Datum();

		System.out.println(date1);
		System.out.printf("%2d.%2d.%d%n", date1.getTag(), date1.getMonat(), date1.getJahr());

		try {
			date2 = new Datum(day, month, year);
			System.out.println(date2);
			System.out.printf("%02d.%02d.%d%n", date2.getTag(), date2.getMonat(), date2.getJahr());
			date2.setJahr(1885);
			System.out.println(date2);
		} catch (TagExistiertNichtException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getFalscherWert());
		} catch (MonatExistiertNichtException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getFalscherWert());
		} catch (JahrExistiertNichtException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getFalscherWert());
		}

		try {
			date3 = new Datum(2, 5, 1970);
			System.out.println(date3);
		} catch (TagExistiertNichtException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getFalscherWert());
		} catch (MonatExistiertNichtException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getFalscherWert());
		} catch (JahrExistiertNichtException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getFalscherWert());
		}

		System.out.println("-------------------");

		Feiertag festtag;
		try {
			festtag = new Feiertag(24, 12, 2014, "Heilig Abend");
			System.out.println(festtag);
			System.out.println((new Datum(1, 1, 2001).compareTo(new Datum(2, 1, 2001))));
		} catch (TagExistiertNichtException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getFalscherWert());
		} catch (MonatExistiertNichtException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getFalscherWert());
		} catch (JahrExistiertNichtException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getFalscherWert());
		}

		System.out.println("-------------------");

		System.out.println(Datum.berechneQuartal(date1));
		System.out.println(Datum.berechneQuartal(date3));

		System.out.println("-------------------");

		istDatumGleich(date1, date1);
		istDatumGleich(date1, date3);

		System.out.println(date1.compareTo(date1));
		System.out.println(date1.compareTo(date3));

	}

	public static void istDatumGleich(Datum d1, Datum d2) {
		if (d1.equals(d2))
			System.out.printf("%s == %s%n", d1, d2);
		else
			System.out.printf("%s != %s%n", d1, d2);
	}

}
