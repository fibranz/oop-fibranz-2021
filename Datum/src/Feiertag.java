
public class Feiertag extends Datum {

	// Attribute

	private String feiertagname;

	// Konstruktoren

	public Feiertag(int tag, int monat, int jahr, String feiertagname)
			throws TagExistiertNichtException, MonatExistiertNichtException, JahrExistiertNichtException {
		super(tag, monat, jahr);
		this.feiertagname = feiertagname;
	}

	// Methoden

	public String getFeiertagname() {
		return feiertagname;
	}

	public void setFeiertagname(String feiertagname) {
		this.feiertagname = feiertagname;
	}

	@Override
	public String toString() {
		return super.toString() + " (" + feiertagname + ")";
	}

}
