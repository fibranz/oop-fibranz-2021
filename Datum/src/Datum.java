
public class Datum implements Comparable<Datum> {

	// **** Attribute ****

	private int tag;
	private int monat;
	private int jahr;

	// **** Konstruktoren ****

	public Datum() {
		tag = 1;
		monat = 1;
		jahr = 1970;
	}

	public Datum(int tag, int monat, int jahr)
			throws TagExistiertNichtException, MonatExistiertNichtException, JahrExistiertNichtException {
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);
	}

	// **** Methoden ****

	// ** Klassenmethoden **

	public static int berechneQuartal(Datum d) {
		int monat = d.getMonat();
		if (monat >= 1 && monat <= 3)
			return 1;
		if (monat >= 4 && monat <= 6)
			return 2;
		if (monat >= 7 && monat <= 9)
			return 3;
		if (monat >= 10 && monat <= 12)
			return 4;
		return 0;// Fehlerfall
	}

	// ** Objektmethode **

	public void setTag(int tag) throws TagExistiertNichtException {
		if (tag < 1 || tag > 31)
			throw new TagExistiertNichtException("Fehler: Tag existiert nicht.", tag);
		this.tag = tag;
	}

	public void setMonat(int monat) throws MonatExistiertNichtException {
		if (monat < 0 || monat > 12)
			throw new MonatExistiertNichtException("Fehler: Monat existiert nicht.", monat);
		this.monat = monat;
	}

	public void setJahr(int jahr) throws JahrExistiertNichtException {
		if (jahr < 1900 || jahr > 2100)
			throw new JahrExistiertNichtException("Fehler: Jahr existiert nicht.", jahr);
		this.jahr = jahr;
	}

	public int getTag() {
		return tag;
	}

	public int getMonat() {
		return monat;
	}

	public int getJahr() {
		return jahr;
	}

	public String toString() {
		return tag + "." + monat + "." + jahr;
	}

	public boolean equals(Object obj) {
		if (obj instanceof Datum) {
			Datum d = (Datum) obj;
			boolean istTagGleich = (this.tag == d.getTag());
			boolean istMonatGleich = (this.monat == d.getMonat());
			boolean istJahrGleich = (this.jahr == d.getJahr());
			if (istTagGleich && istMonatGleich && istJahrGleich)
				return true;
			else
				return false;
		}
		return false;
	}

	@Override
	public int compareTo(Datum o) {
		if (this.jahr > o.getJahr())
			return 1;
		if (this.jahr < o.getJahr())
			return -1;
		// -> Jahre gleich
		if (this.monat > o.getMonat())
			return 1;
		if (this.monat < o.getMonat())
			return -1;
		// -> Monate gleich
		if (this.tag > o.getTag())
			return 1;
		if (this.tag < o.getTag())
			return -1;
		// -> Tage gleich, d.h. beide Daten identisch
		return 0;
	}

}
