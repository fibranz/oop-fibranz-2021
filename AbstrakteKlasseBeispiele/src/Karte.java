
public abstract class Karte {

	// Attribute

	protected String empfaenger;
	protected String geschlecht;

	// Konstruktoren

	public Karte(String empfaenger, String geschlecht) {
		this.empfaenger = empfaenger;
		this.geschlecht = geschlecht;
	}

	// Methoden

	public abstract void gruss();

}
