
public class Feiertag extends Karte {

	// Attribute

	// Konstruktoren

	public Feiertag(String empfaenger, String geschlecht) {
		super(empfaenger, geschlecht);
	}

	// Methoden

	@Override
	public void gruss() {
		if (geschlecht.equals("maennlich"))
			System.out.println("Lieber Herr " + empfaenger + ",");
		else
			System.out.println("Liebe Frau " + empfaenger + ",");
		System.out.println("frohe Feiertage!");
	}

}
