
public class KarteTest {

	public static void main(String[] args) {

		// Karte k1 = new Karte();// FEHLER
		// von einer abstrakten Klasse kann man keine Objekte erstellen

		Karte[] kListe = new Karte[4];

		Geburtstag g1 = new Geburtstag("Max Muster", "maennlich", 17);
		g1.gruss();
		Geburtstag g2 = new Geburtstag("Anna Muster", "weiblich", 19);
		g2.gruss();
		Feiertag f1 = new Feiertag("Paul Muster", "maennlich");
		f1.gruss();
		Feiertag f2 = new Feiertag("Frieda Muster", "weiblich");
		f2.gruss();

		System.out.println("---------------------");

		kListe[0] = g1;
		kListe[1] = g2;
		kListe[2] = f1;
		kListe[3] = f2;

		for (Karte karte : kListe)
			karte.gruss();

	}

}
