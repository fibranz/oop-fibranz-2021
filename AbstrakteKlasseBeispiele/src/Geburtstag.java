
public class Geburtstag extends Karte {

	// Attribute

	private int alter;

	// Konstruktoren
	public Geburtstag(String empfaenger, String geschlecht, int alter) {
		super(empfaenger, geschlecht);
		this.alter = alter;
	}

	// Methoden

	@Override
	public void gruss() {
		if (geschlecht.equals("maennlich"))
			System.out.println("Lieber Herr " + empfaenger + ",");
		else
			System.out.println("Liebe Frau " + empfaenger + ",");
		System.out.println("alles Gute zum Geburtstag!");
	}

}
